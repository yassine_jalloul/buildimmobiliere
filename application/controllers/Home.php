<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(0);
    }

    public function index() {
        $gets = $this->input->get();
        $projets = Get_all_projects_2();
        $data['projets'] = isset($projets['items']) ? $projets['items'] : array();
        $data['search'] = CURL_PARAM_SEARCH();
        $property_types = isset($data['search']['property_types']) ? $data['search']['property_types'] : array();
        $data['type_biens'] = $property_types;
        $vocations = isset($data['search']['vocations']) ? $data['search']['vocations'] : array();
        $data['vocations'] = $vocations;
        $gouvernorats = isset($data['search']['gouvernorats']) ? $data['search']['gouvernorats'] : array();
        $data['gouvernorats'] = $gouvernorats;
        $delegations = isset($data['search']['delegations']) ? $data['search']['delegations'] : array();
        $data['delegations'] = $delegations;
        //var_dump($data['projets']);
        $data['title'] = 'Welcome to Build Immobilière | Build Immobilière';
        $data['contents'] = 'FrontOffice/home';
        $this->load->view('FrontOffice/index', $data);
    }
    

}
