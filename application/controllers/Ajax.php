<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Ajax extends CI_Controller
{
    public function __construct()
    {

        parent::__construct();
        $this->domain = SITE;
        error_reporting(0);
    }

    public function detail($id = NULL)
    {
        $this->load->helper('cookie');
        $list = get_cookie("compare_list");
        $list = ($list) ? json_decode($list, true) : array();
        //$this->load->library('user_agent');
        $data['details'] = curl_properties_detail($id);
        $data['in_compare_list'] = isset($list[$id]) && $list[$id] ? true : false;
        //$data['is_mobile'] = $this->agent->is_mobile();
        $this->load->view('FrontOffice/detail_modal', $data);
    }

    public function pdf($id = NULL)
    {
        $p = curl_properties_detail($id);
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetHeaderData(null, 0, $p["name"], $p["name"]);
        $pdf->SetTitle($p["name"]);
        $pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(20);
        $pdf->setFooterMargin(20);
        $pdf->SetSubject($p["name"]);
        $pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('Concept Lab');
        $pdf->SetDisplayMode('real', 'default');
        //$html = $this->load->view('Frontend/print_plans', ['details'=>$p, "domain"=>$this->domain], true);
        $name = $p['name'];
        $k = 0;
        foreach($p["plans_images"] as $image) {
            $k ++;
            $image = $this->domain . "/" . $image["web_path"];
            $pdf->AddPage();
            $html = "<p style=\"text-align: center\"><h1>$name Plan $k</h1></p>";
            $h = "<p style=\"text-align: center; margin-top: 30px;\"><img src=\"$image\" style='width: 100%' border=\"0\"></p>";
            $h = $html . $h;
            $pdf->writeHTML($h , true, false, true, false, '');
        }
        $pdf->Output($p["name"].'.pdf', 'I');
    }

}