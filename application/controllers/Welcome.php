<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('recaptcha');
//        $this->load->library('session');
//        $this->load->helper('captcha');
    }

    public function index() {
        $gets = $this->input->get();
        $projets = Get_all_projects();
        $data['projets'] = isset($projets['items']) ? $projets['items'] : array();
        $data['search'] = CURL_PARAM_SEARCH();
        $property_types = isset($data['search']['property_types']) ? $data['search']['property_types'] : array();
        $data['type_biens'] = $property_types;
        $vocations = isset($data['search']['vocations']) ? $data['search']['vocations'] : array();
        $data['vocations'] = $vocations;
        $gouvernorats = isset($data['search']['gouvernorats']) ? $data['search']['gouvernorats'] : array();
        $data['gouvernorats'] = $gouvernorats;
        $delegations = isset($data['search']['delegations']) ? $data['search']['delegations'] : array();
        $data['delegations'] = $delegations;
        //var_dump($data['projets']);
//        $vals = array(
//            'img_path'      => './captcha/',
//            'img_url'       => base_url('captcha/'),
//            'font_path'     => base_url('system/fonts/texb.ttf'),
//            'img_width'     => '150',
//            'img_height'    => 30,
//            'expiration'    => 7200,
//            'word_length'   => 8,
//            'font_size'     => 16,
//            'img_id'        => 'Imageid',
//            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
//
//            // White background and border, black text and red grid
//            'colors'        => array(
//                'background' => array(255, 255, 255),
//                'border' => array(255, 255, 255),
//                'text' => array(0, 0, 0),
//                'grid' => array(255, 40, 40)
//            )
//        );
//
//        $captcha = create_captcha($vals);
//        $this->session->unset_userdata('captchaCode');
//        $this->session->set_userdata('captchaCode', $captcha['word']);
//        $data['captchaImg'] = $captcha['image'];
        $data['widget'] = $this->recaptcha->getWidget();
        $data['script'] = $this->recaptcha->getScriptTag();
        $data['title'] = 'Welcome to Build Immobilière | Build Immobilière';
        $data['contents'] = 'FrontOffice/home';
        $this->load->view('FrontOffice/index', $data);
    }

}
