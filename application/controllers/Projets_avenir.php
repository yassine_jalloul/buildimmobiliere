<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Projets_avenir extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        $this->per_page = 9;
        error_reporting(0);
        $this->load->library('recaptcha');
//        $this->load->library('session');
//        $this->load->helper('captcha');
    }

    public function index() {
        $data['title'] = 'Erreur 404 | Build Immobilière';
        $data['contents'] = 'FrontOffice/404';
        $this->load->view('FrontOffice/index', $data);
    }

    public function detail($id) {
        if (isset($gets['page'])) {
            unset($gets['page']);
        }
        $data['page'] = 1;
        if ($this->uri->segment(4)):
            $data['page'] = $this->uri->segment(4);
            $gets['page'] = $this->uri->segment(4);
        else:
            $gets['page'] = 1;
        endif;
        $show = '';
        if (isset($_GET['show'])):
            $show = $this->input->get('show');
        endif;
        $projets = Get_all_projects();
        $data['projets'] = isset($projets['items']) ? $projets['items'] : array();
        //$data['search'] = json_decode($result4, true);
        $data['search'] = CURL_PARAM_SEARCH();
        $property_types = isset($data['search']['property_types']) ? $data['search']['property_types'] : array();
        $data['type_biens'] = $property_types;
        $vocations = isset($data['search']['vocations']) ? $data['search']['vocations'] : array();
        $data['vocations'] = $vocations;
        $gouvernorats = isset($data['search']['gouvernorats']) ? $data['search']['gouvernorats'] : array();
        $data['gouvernorats'] = $gouvernorats;
        $delegations = isset($data['search']['delegations']) ? $data['search']['delegations'] : array();
        $data['delegations'] = $delegations;
        $data['projet'] = Project_by_id($id);
        $data['properties'] = Properties_by_project($id, $gets['page']);
        $data['size'] = $data['properties']['total_count'];
        $properties = isset($data['properties']['items']) ? $data['properties']['items'] : array();
        $data['properties'] = $properties;
        if ($data['size'] > $this->per_page):
            $pagination = pagination();
            $pagination['base_url'] = base_url('projets_avenir/detail/' . $this->uri->segment(3));
            $pagination['total_rows'] = $data['size'];
            $this->pagination->initialize($pagination);
            $data["links"] = $this->pagination->create_links();
        endif;
        //var_dump($data['properties']);
//        $vals = array(
//            'img_path'      => './captcha/',
//            'img_url'       => base_url('captcha/'),
//            'font_path'     => base_url('system/fonts/texb.ttf'),
//            'img_width'     => '150',
//            'img_height'    => 30,
//            'expiration'    => 7200,
//            'word_length'   => 8,
//            'font_size'     => 16,
//            'img_id'        => 'Imageid',
//            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
//
//            // White background and border, black text and red grid
//            'colors'        => array(
//                'background' => array(255, 255, 255),
//                'border' => array(255, 255, 255),
//                'text' => array(0, 0, 0),
//                'grid' => array(255, 40, 40)
//            )
//        );
//        $captcha = create_captcha($vals);
//        $this->session->unset_userdata('captchaCode');
//        $this->session->set_userdata('captchaCode', $captcha['word']);
//        $data['captchaImg'] = $captcha['image'];
        $data['widget'] = $this->recaptcha->getWidget();
        $data['script'] = $this->recaptcha->getScriptTag();
        $data['properties_items'] = 'FrontOffice/list_biens';
        if ($show == 'grid') {
            $data['properties_items'] = 'FrontOffice/list_biens_row';
        }

        if ($this->input->is_ajax_request()) {
            $this->load->view($data['properties_items'], $data);
        } else {
            if (empty($data['projet'])):
                $data['title'] = 'Erreur 404 | Build Immobilière';
                $data['contents'] = 'FrontOffice/404';
            else:
                $data['title'] = $data['projet']['titre'] . ' | Build Immobilière';
                $data['contents'] = 'FrontOffice/projets_avenir';
            endif;
            $this->load->view('FrontOffice/index', $data);
        }
    }

    public function search($id) {
        if (isset($gets['page'])) {
            unset($gets['page']);
        }
        $data['page'] = 1;
        if ($this->uri->segment(4)):
            $data['page'] = $this->uri->segment(4);
            $gets['page'] = $this->uri->segment(4);
        else:
            $gets['page'] = 1;
        endif;
        $show = '';
        if (isset($_GET['show'])):
            $show = $this->input->get('show');
        endif;
        $projets = Get_all_projects();
        $data['projets'] = isset($projets['items']) ? $projets['items'] : array();
        $data['search'] = CURL_PARAM_SEARCH();
        $property_types = isset($data['search']['property_types']) ? $data['search']['property_types'] : array();
        $data['type_biens'] = $property_types;
        $vocations = isset($data['search']['vocations']) ? $data['search']['vocations'] : array();
        $data['vocations'] = $vocations;
        $gouvernorats = isset($data['search']['gouvernorats']) ? $data['search']['gouvernorats'] : array();
        $data['gouvernorats'] = $gouvernorats;
        $delegations = isset($data['search']['delegations']) ? $data['search']['delegations'] : array();
        $data['delegations'] = $delegations;
        $data['properties'] = curl_search($_GET, $gets['page'], $id);
        //var_dump($data['biens']);
        $data['size'] = $data['properties']['total_count'];
        $plans = isset($data['properties']['items']) ? $data['properties']['items'] : array();
        $data['properties'] = $plans;
        if ($data['size'] > $this->per_page):
            $pagination = pagination();
            $pagination['base_url'] = base_url('projets_avenir/search/' . $id);
            $pagination['total_rows'] = $data['size'];
            $this->pagination->initialize($pagination);
            $data["links"] = $this->pagination->create_links();
        //var_dump($data['links']);
        endif;
        //var_dump($data['size']);
        $data['properties_items'] = 'FrontOffice/list_biens';
        if ($show == 'grid') {
            $data['properties_items'] = 'FrontOffice/list_biens_row';
        }

        if ($this->input->is_ajax_request()) {
            $this->load->view($data['properties_items'], $data);
        } else {
            if (empty($data['projet'])):
                $data['title'] = 'Erreur 404 | Build Immobilière';
                $data['contents'] = 'FrontOffice/404';
            else:
                $data['title'] = $data['projet']['titre'] . ' | Build Immobilière';
                $data['contents'] = 'FrontOffice/projets_avenir';
            endif;
            $this->load->view('FrontOffice/index', $data);
        }
    }

}
