<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('recaptcha');
//        $this->load->library('session');
//        $this->load->helper('captcha');
    }

    public function index() {
        $projets = Get_all_projects();
        $data['projets'] = isset($projets['items']) ? $projets['items'] : array();
        $data['title'] = 'Contact | Build Immobilière';
//        $vals = array(
//            'img_path'      => './captcha/',
//            'img_url'       => base_url('captcha/'),
//            'font_path'     => base_url('system/fonts/texb.ttf'),
//            'img_width'     => '150',
//            'img_height'    => 30,
//            'expiration'    => 7200,
//            'word_length'   => 8,
//            'font_size'     => 16,
//            'img_id'        => 'Imageid',
//            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
//
//            // White background and border, black text and red grid
//            'colors'        => array(
//                'background' => array(255, 255, 255),
//                'border' => array(255, 255, 255),
//                'text' => array(0, 0, 0),
//                'grid' => array(255, 40, 40)
//            )
//        );
//
//        $captcha = create_captcha($vals);
//        $this->session->unset_userdata('captchaCode');
//        $this->session->set_userdata('captchaCode', $captcha['word']);
//        $data['captchaImg'] = $captcha['image'];
        $data['widget'] = $this->recaptcha->getWidget();
        $data['script'] = $this->recaptcha->getScriptTag();
        $data['contents'] = 'FrontOffice/contact';
        $this->load->view('FrontOffice/index', $data);
    }

    function parse_incoming() {
        global $_GET, $_POST, $HTTP_CLIENT_IP, $REQUEST_METHOD, $REMOTE_ADDR, $HTTP_PROXY_USER, $HTTP_X_FORWARDED_FOR;
        $return = array();

        if (is_array($_GET)) {
            while (list($k, $v) = each($_GET)) {
                if (is_array($_GET[$k])) {
                    while (list($k2, $v2) = each($_GET[$k])) {
                        $return[$k][$this->clean_key($k2)] = $this->clean_value($v2);
                    }
                } else {
                    $return[$this->clean_key($k)] = $this->clean_value($v);
                }
            }
        }

        // Overwrite GET data with post data

        if (is_array($_POST)) {
            while (list($k, $v) = each($_POST)) {
                if (is_array($_POST[$k])) {
                    while (list($k2, $v2) = each($_POST[$k])) {
                        $return[$k][$this->clean_key($k2)] = $this->clean_value($v2);
                    }
                } else {
                    $return[$this->clean_key($k)] = $this->clean_value($v);
                }
            }
        }
        $return['REQUEST_METHOD'] = $_SERVER['REQUEST_METHOD'];
        $return['IP_ADDRESS'] = $_SERVER['SERVER_ADDR'];
        $return['IP_CLIENT'] = $_SERVER['REMOTE_ADDR'];
        $return['IP_CLIENT'] = $_SERVER['HTTP_X_FORWARDED_FOR'] ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        define('IS_POST', $return['REQUEST_METHOD'] == 'POST');


        return $return;
    }

    /**
     * called in parse_incoming
     */
    private function clean_key($key) {
        return $key;
    }

    /**
     * called in parse_incoming
     */
    private function clean_value($val) {
        $val = html_entity_decode($val);
        $val = strip_tags($val);
        if (get_magic_quotes_gpc() != 0) {
            $val = stripslashes($val);
        }
        return $val;
    }

    public function send_mail() {
        $input = $this->parse_incoming();
        //$to = 'yassine.jalloul@gmail.com';
        $to = 'moufid.arouay@smg.com.tn , Hela.benaziza@smg.com.tn';
        //$inputCaptcha = $input['captcha'];
        //$sessCaptcha = $this->session->userdata('captchaCode');
        $recaptcha = $this->input->post('g-recaptcha-response');
        //var_dump($sessCaptcha);
        if (!empty($recaptcha)) {
            $response = $this->recaptcha->verifyResponse($recaptcha);
        if(isset($response['success']) and $response['success'] === true){

            if ($input['type-form'] == 'devis-form'):
                $firstname = $input['name'];
                $phone = $input['phone'];
                $email = $input['email'];
                $message = $input['message'];
                $bien_name = $input['name_bien'];
                $bien_id = $input['id_bien'];
                $project_name = $input['project_name'];
                $project_id = $input['project_id'];
                $email_subject = 'Formulaire de devis';
                $email_body = "Vous avez reçu un email de la part de votre site buildimmobiliere.tn.\n\n" . "Voici les détails:\n\n Nom et prènom: $firstname\n\n Téléphone: $phone\n\n E-mail: $email\n\n Nom du bien: $bien_name\n\n Projet: $project_name\n\n Message:\n$message";
                $headers = "From: buildimmobiliere.tn\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
                $headers .= "Reply-To: $email";
                $data = array(
                    'type-form' => 'devis',
                    'name' => $firstname,
                    'email' => $email,
                    'tel' => $phone,
                    'propertyId' => $bien_id,
                    'projetId' => $project_id,
                );
                $result = CURL_send_mail('api/devis-message', $data);
                if(mail($to, $email_subject, $email_body, $headers)):
                    echo json_encode(array(
                        'status'=>1,
                        'msg'=>'OK'
                    ));
                else:
                    echo json_encode(array(
                        'status'=>0,
                        'msg'=>'Error !!'
                    ));
                    return false;
                endif;
            elseif ($input['type-form'] == 'contact-form'):
                $firstname = $input['name'];
                $phone = $input['tel'];
                $email = $input['email'];
                $sujet = $input['sujet'];
                $message = $input['message'];
                $email_subject = 'Formulaire de contact';
                $email_body = "Vous avez reçu un email de la part de votre site buildimmobiliere.tn.\n\n" . "Voici les détails:\n\n Nom et prènom: $firstname\n\n Téléphone: $phone\n\n E-mail: $email\n\n Sujet: $sujet\n\n  Message:\n$message";
                $headers = "From: buildimmobiliere.tn\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
                $headers .= "Reply-To: $email";
                $data = array(
                    'type-form' => 'contact',
                    'name' => $firstname,
                    'email' => $email,
                    'subject' => $sujet,
                    'content' => $message
                );
                $result = CURL_send_mail('api/contact-message', $data);
                if(mail($to, $email_subject, $email_body, $headers)):
                    echo json_encode(array(
                        'status'=>1,
                        'msg'=>'OK'
                    ));
                else:
                    echo json_encode(array(
                        'status'=>0,
                        'msg'=>'Error !!'
                    ));
                    return false;
                endif;
            endif;

        }else{
            echo json_encode(array(
                'status'=>2,
                'msg'=>'Error !!'
            ));
            return false;
        }
        }else{
            echo json_encode(array(
                'status'=>2,
                'msg'=>'Error !!'
            ));
            return false;
        }
    }

////    public function refresh(){
////        $vals = array(
////            'img_path'      => './captcha/',
////            'img_url'       => base_url('captcha/'),
////            'font_path'     => base_url('system/fonts/texb.ttf'),
////            'img_width'     => '150',
////            'img_height'    => 30,
////            'expiration'    => 7200,
////            'word_length'   => 8,
////            'font_size'     => 16,
////            'img_id'        => 'Imageid',
////            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
////            'colors'        => array(
////                'background' => array(255, 255, 255),
////                'border' => array(255, 255, 255),
////                'text' => array(0, 0, 0),
////                'grid' => array(255, 40, 40)
////            )
////        );
////
////        $captcha = create_captcha($vals);
////        $this->session->unset_userdata('captchaCode');
////        $this->session->set_userdata('captchaCode',$captcha['word']);
////        echo $captcha['image'];
//    }

}
