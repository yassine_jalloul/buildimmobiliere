<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Biens extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        $this->per_page = 9;
        error_reporting(0);
    }

    public function index() {
        $data['title'] = 'Dahmen | 404';
        $data['contents'] = 'FrontOffice/404';
        $this->load->view('FrontOffice/index', $data);
    }

    /* private function CURL() {
      $gets = $this->input->get();
      $gets['_token'] = IMMO_TOKEN;
      $url = SITE . 'api/properties';
      $query = http_build_query($gets);
      $url .= "?$query";
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($curl);
      $result = utf8_decode($result);
      return json_decode($result, true);
      }
      public function index() {
      $data['page'] = 1;
      if ($this->uri->segment(3)):
      $data['page'] = $this->uri->segment(3);
      endif;
      $data['biens'] = $this->CURL();
      $data['size'] = sizeof($data['biens']);
      //var_dump($data['biens']);
      if (sizeof($data['biens']) > $this->per_page):
      $pagination = $this->pagination();
      $pagination['base_url'] = base_url() . 'biens/liste';
      $pagination['total_rows'] = sizeof($data['biens']);
      $this->pagination->initialize($pagination);
      $new_properties = array();
      if ($data['page'] == 1):
      for ($i = 0; $i < $this->per_page; $i++):
      $new_properties[] = $data['biens'][$i];
      endfor;
      else:
      for ($i = ($data['page'] - 1) * $this->per_page; $i < $this->per_page * $data['page']; $i++):
      if ($i == sizeof($data['biens'])):
      break;
      endif;
      $new_properties[] = $data['biens'][$i];
      endfor;
      endif;
      $data['biens'] = $new_properties;
      $data["links"] = $this->pagination->create_links();
      endif;
      if (empty($data['biens'])):
      $data['title'] = 'Dahmen | 404';
      $data['content'] = 'FrontOffice/404';
      else:
      $data['title'] = 'Dahmen | Liste des biens';
      $data['titre_page'] = 'Liste des biens';
      $data['contents'] = 'FrontOffice/list_biens';
      endif;
      $this->load->view('FrontOffice/index', $data);
      }
      public function liste() {
      $data['page'] = 1;
      if ($this->uri->segment(3)):
      $data['page'] = $this->uri->segment(3);
      endif;
      $data['biens'] = $this->CURL();
      $data['size'] = sizeof($data['biens']);
      //var_dump($data['biens']);
      if (sizeof($data['biens']) > $this->per_page):
      $pagination = $this->pagination();
      $pagination['base_url'] = base_url() . 'biens/liste';
      $pagination['total_rows'] = sizeof($data['biens']);
      $this->pagination->initialize($pagination);
      $new_properties = array();
      if ($data['page'] == 1):
      for ($i = 0; $i < $this->per_page; $i++):
      $new_properties[] = $data['biens'][$i];
      endfor;
      else:
      for ($i = ($data['page'] - 1) * $this->per_page; $i < $this->per_page * $data['page']; $i++):
      if ($i == sizeof($data['biens'])):
      break;
      endif;
      $new_properties[] = $data['biens'][$i];
      endfor;
      endif;
      $data['biens'] = $new_properties;
      $data["links"] = $this->pagination->create_links();
      endif;
      if (empty($data['biens'])):
      $data['title'] = 'Dahmen | 404';
      $data['content'] = 'FrontOffice/404';
      else:
      $data['title'] = 'Dahmen | Liste des biens';
      $data['titre_page'] = 'Liste des biens';
      $data['contents'] = 'FrontOffice/list_biens';
      endif;
      $this->load->view('FrontOffice/index', $data);
      }

      public function grid() {
      $data['page'] = 1;
      if ($this->uri->segment(3)):
      $data['page'] = $this->uri->segment(3);
      endif;
      $data['biens'] = $this->CURL();
      $data['size'] = sizeof($data['biens']);
      //var_dump($data['biens']);
      if (sizeof($data['biens']) > $this->per_page):
      $pagination = $this->pagination();
      $pagination['base_url'] = base_url() . 'biens/grid';
      $pagination['total_rows'] = sizeof($data['biens']);
      $this->pagination->initialize($pagination);
      $new_properties = array();
      if ($data['page'] == 1):
      for ($i = 0; $i < $this->per_page; $i++):
      $new_properties[] = $data['biens'][$i];
      endfor;
      else:
      for ($i = ($data['page'] - 1) * $this->per_page; $i < $this->per_page * $data['page']; $i++):
      if ($i == sizeof($data['biens'])):
      break;
      endif;
      $new_properties[] = $data['biens'][$i];
      endfor;
      endif;
      $data['biens'] = $new_properties;
      $data["links"] = $this->pagination->create_links();
      endif;
      if (empty($data['biens'])):
      $data['title'] = 'Dahmen | 404';
      $data['content'] = 'FrontOffice/404';
      else:
      $data['title'] = 'Dahmen | Liste des biens';
      $data['titre_page'] = 'Liste des biens';
      $data['contents'] = 'FrontOffice/list_biens_row';
      endif;
      $this->load->view('FrontOffice/index', $data);
      }

      private function pagination() {
      $config['per_page'] = $this->per_page;
      $config['num_links'] = 2;
      $config["uri_segment"] = 3;
      $config['num_links'] = 2;
      $config['use_page_numbers'] = TRUE;
      $config['full_tag_open'] = '<div class="offer-pagination margin-top-30">';
      $config['full_tag_close'] = '</div>';

      $config['first_link'] = '<<';

      $config['last_link'] = '>>';

      $config['next_link'] = '<i class="jfont">&#xe802;</i>';

      $config['prev_link'] = '<i class="jfont">&#xe800;</i>';

      $config['cur_tag_open'] = '<a class="active">';
      $config['cur_tag_close'] = '</a>';
      return $config;
      } */

    private function CURL_PARAM_SEARCH() {
        $gets = $this->input->get();
        $gets['_token'] = IMMO_TOKEN;
        //$url = SITE . 'api/19/properties-by-project';
        ########CURL search#########
        $url = SITE . 'api/params-search';
        $query = http_build_query($gets);
        $url .= "?$query";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        $result = utf8_decode($result);
        return json_decode($result, true);
    }

    public function detail($id = NULL) {
        $gets = $this->input->get();
        $gets['_token'] = IMMO_TOKEN;
        $url = SITE . 'api/property-details';
        $query = http_build_query($gets);
        $url .= "?$query&id=$id";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        $result = utf8_decode($result);
        ##################
        $url = SITE . 'api/properties';
        unset($gets['vedettes']);
        $gets['limit'] = 6;
        $gets['order'] = 'date';
        $gets['srt'] = 'DESC';
        $query = http_build_query($gets);
        $url .= "?$query";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result2 = curl_exec($curl);
        curl_close($curl);
        $result2 = utf8_decode($result2);
        ##################
        $data['search'] = $this->CURL_PARAM_SEARCH();
        $property_types = isset($data['search']['property_types']) ? $data['search']['property_types'] : array();
        $data['type_biens'] = $property_types;
        $vocations = isset($data['search']['vocations']) ? $data['search']['vocations'] : array();
        $data['vocations'] = $vocations;
        $gouvernorats = isset($data['search']['gouvernorats']) ? $data['search']['gouvernorats'] : array();
        $data['gouvernorats'] = $gouvernorats;
        $delegations = isset($data['search']['delegations']) ? $data['search']['delegations'] : array();
        $data['delegations'] = $delegations;
        $data['properties'] = json_decode($result, true);
        $data['recentProperties'] = json_decode($result2, true);
        $plans = isset($data['recentProperties']['items']) ? $data['recentProperties']['items'] : array();
        $data['recentProperties'] = $plans;
        //var_dump($data['properties']);
        $data['title'] = 'Dahmen | Détail';
        if (!empty($data['properties'])):
            $data['contents'] = 'FrontOffice/detail_bien';
            $this->load->view('FrontOffice/index', $data);
        else:
            $data['contents'] = 'FrontOffice/404';
            $this->load->view('FrontOffice/index', $data);
        endif;
    }

}
