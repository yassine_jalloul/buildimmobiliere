<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        $this->per_page = 9;
        error_reporting(0);
    }

    public function index() {
        $this->liste();
    }

    public function liste($id) {
        if (isset($gets['page'])) {
            unset($gets['page']);
        }
        $data['page'] = 1;
        if ($this->uri->segment(3)):
            $data['page'] = $this->uri->segment(3);
            $gets['page'] = $this->uri->segment(3);
        else:
            $gets['page'] = 1;
        endif;
        $show = '';
        if (isset($_GET['show'])):
            $show = $this->input->get('show');
        endif;
        $projets = Get_all_projects();
        $data['projets'] = isset($projets['items']) ? $projets['items'] : array();
        $data['search'] = CURL_PARAM_SEARCH();
        $property_types = isset($data['search']['property_types']) ? $data['search']['property_types'] : array();
        $data['type_biens'] = $property_types;
        $vocations = isset($data['search']['vocations']) ? $data['search']['vocations'] : array();
        $data['vocations'] = $vocations;
        $gouvernorats = isset($data['search']['gouvernorats']) ? $data['search']['gouvernorats'] : array();
        $data['gouvernorats'] = $gouvernorats;
        $delegations = isset($data['search']['delegations']) ? $data['search']['delegations'] : array();
        $data['delegations'] = $delegations;
        $data['properties'] = curl_search($_GET, $gets['page'], $id);
        //var_dump($data['biens']);
        $data['size'] = $data['properties']['total_count'];
        $plans = isset($data['properties']['items']) ? $data['properties']['items'] : array();
        $data['properties'] = $plans;
        if ($data['size'] > $this->per_page):
            $pagination = pagination();
            $pagination['base_url'] = base_url('search/liste/' . $id);
            $pagination['total_rows'] = $data['size'];
            $pagination['uri_segment'] = 3;
            $this->pagination->initialize($pagination);
            $data["links"] = $this->pagination->create_links();
        //var_dump($data['links']);
        endif;
        //var_dump($data['properties']);
        $data['properties_items'] = 'FrontOffice/list_biens';
        if ($show == 'grid') {
            $data['properties_items'] = 'FrontOffice/list_biens_row';
        }
        if (empty($data['properties'])):
            $data['title'] = 'Search | Build Immobilière';
            $data['contents'] = 'FrontOffice/vide';
        else:
            $data['title'] = 'Search | Build Immobilière';
            $data['contents'] = 'FrontOffice/search';
        endif;
        $this->load->view('FrontOffice/index', $data);
    }

}
