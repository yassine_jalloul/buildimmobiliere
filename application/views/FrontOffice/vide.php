<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="short-image no-padding blog-short-title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-12 short-image-title">
                <h1 class="second-color">Oups!</h1>
                <div class="short-title-separator"></div>
            </div>
        </div>
    </div>

</section>

<section class="section-light section-top-shadow">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="huge-header">Oups<span class="special-color">!</span></h1>
                <p class="margin-top-105 centered-text">Cette catégorie est en train d'être alimentée. Revenez très bientôt et vous y trouverez les meilleures annonces.</p>
                <p class="centered-text">Merci pour votre compréhension</p>
                <p class="centered-text">L'équipe Build Immobilière</p>
                <p class="centered-text">Aller à la <strong><a href="<?php echo base_url(); ?>">Page d'accueil</a></strong> ou revenir à la <strong><a href="javascript:history.back()">Page précédente</a></strong></p>
            </div>
        </div>
    </div>
</section>