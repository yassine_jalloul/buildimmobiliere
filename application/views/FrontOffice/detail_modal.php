<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!empty($details)) {
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 40px;"><span
                    aria-hidden="true">&times;</span></button>
        <?php
        if (isset($details['name'])):
            ?>
            <h3 class="text-center">
                <?php if (isset($details['property_sub_type'][0]['name'])){ ?>
                    <?= $details['property_sub_type'][0]['name'] ?>: <?= $details['name'] ?>
                <?php }else{ ?>
                    <?= $details['property_type']['name'] ?>: <?= $details['name'] ?>
                <?php } ?>
            </h3>
            <?php
        endif;
        ?>
        <ul class="condition clearfix list-inline text-center" style="margin-top: 20px;padding: 0 30px;">
            <?php if (isset($details['property_type']['name'])):
                $array_duplex = array(
                    "penthouse", "appartement duplex"
                );
                $subTypeStr = "";
                if (isset($details['property_sub_type']) && count($details['property_sub_type'])){
                    foreach ($details['property_sub_type'] as $subType){
                        if(in_array(strtolower($subType["name"]), $array_duplex)){
                            $subTypeStr = $subType["name"];
                            break;
                        }
                    }
                }
                ?>
                <li class="bordered-right">
                    <span>
                        <strong class="text-uppercase">Type : </strong>
                        <?= $subTypeStr ? " " . $subTypeStr : $details['property_type']['name'] ?>
                    </span>
                </li>
            <?php endif; ?>
            <?php if (isset($details['total_pieces'])): ?>
                <li class="bordered-right">
                    <span><strong class="text-uppercase">Nombre de pièces : </strong>S+<?php echo $details['total_pieces']; ?></span>
                </li>
            <?php endif; ?>
            <?php if (isset($details['etage'])): ?>
                <li class="bordered-right">
                    <span><strong class="text-uppercase">étage : </strong><?php echo $details['etage']; ?></span>
                </li>
            <?php endif; ?>
            <?php if (isset($details['surface_terrain'])): ?>
                <li class="bordered-right">
                    <span><strong class="text-uppercase">Surface terrain : </strong><?php echo $details['surface_terrain']; ?> m²</span>
                </li>
            <?php endif; ?>
            <?php if (isset($details['surface_hors_oeuvre'])): ?>
                <li class="bordered-right">
                    <span><strong class="text-uppercase">Surface H.O : </strong><?php echo $details['surface_hors_oeuvre']; ?> m²</span>
                </li>
            <?php endif; ?>
            <?php if (isset($details['surface_terrasse'])): ?>
                <li class="bordered-right">
                    <span><strong class="text-uppercase">Surface Terrasse : </strong><?php echo $details['surface_terrasse']; ?> m²</span>
                </li>
            <?php endif; ?>
            <?php if (isset($details['surface_jardin'])): ?>
                <li class="bordered-right">
                    <span><strong class="text-uppercase">Surface Jardin : </strong><?php echo $details['surface_jardin']; ?> m²</span>
                </li>
            <?php endif; ?>
            <?php if (isset($details['surface_piscine'])): ?>
                <li class="bordered-right">
                    <span><strong class="text-uppercase">Surface Piscine : </strong><?php echo $details['surface_piscine']; ?> m²</span>
                </li>
            <?php endif; ?>
        </ul>
    </div>
    <div class="modal-body clearfix text-center">
        <div class="m-b-20  clearfix">
            <button data-toggle="tooltip" title="Contactez nous" type="button" class="btn btn-contact-modal btn-list" onclick="showFormContact()">
                <i class="flaticon flaticon1-contact"></i>
            </button>
            <?php
            if (isset($details['plans_images']) && count($details['plans_images'])) {
                ?>
                <button data-toggle="tooltip" title="Imprimer" class="btn btn-list" onclick="printJS({printable:'<?= base_url() ?>ajax/pdf/<?= $details["id"] ?>', type:'pdf', showModal:true})">
                    <i class="flaticon flaticon1-print"></i>
                </button>
            <?php
            }
            ?>
            <button data-toggle="tooltip" title="Ajouter à la comparaison" type="button" class="btn btn-list btn-grey <?= ($in_compare_list) ? "clicked" : "" ?>" onclick="addRemoveToCompareList(this, '<?=$details["id"]?>')">
                <i class="flaticon flaticon1-compare"></i>
            </button>
            <button  data-toggle="modal" data-target="#visite360" data-toggle="tooltip" title="Visite 360°" type="button" class="btn btn-list">
                <i class="flaticon flaticon1-360"></i>
            </button>
        </div>
        <form  id="devisForm1" method="POST" novalidate style="display: none;" class="col-md-12 my-3 contactForm"  onsubmit="return submitContactProperty(this);">
            <div class="row">
                <div class="col-md-12 my-2" style="text-align: center;">
                    <div class="col-md-12">
                        <h3>
                            <a href="#" onclick="return false;">Contacter</a>
                        </h3>
                    </div>
                    <div class="form-group name col-md-12">
                        <input type="text" name="name" class="input-text form-control" placeholder="Nom et prénom" required>
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group email col-md-12">
                        <input type="email" name="email" class="input-text form-control" placeholder="Email" required>
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group number col-md-12">
                        <input type="text" name="phone" class="input-text form-control" placeholder="Téléphone" required>
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group message col-md-12">
                        <textarea class="input-text form-control" name="message" placeholder="Je suis intéressé par cet appartement et voudrais le visiter..." required></textarea>
                        <p class="help-block text-danger"></p>
                    </div>
                    <input type="hidden" value="<?= $details['projet']['titre']; ?>" name="projet_name"/>
                    <input type="hidden" value="<?= $details['name'] ?>" name="bien_name"/>
                    <input type="hidden" value="<?= $details['id'] ?>" name="id_bien"/>
                    <div class="col-md-12">
                        <button type="submit" class="detail-property-link btn btn-primary">Envoyer</button>
                        <button type="button" class="btn btn-danger" onclick="showFormContact()">Fermer</button>
                    </div>
                </div>
            </div>
        </form>
        <?php if (isset($details['images'])&&!empty($details['images'])): ?>
            <div class="col mt-2">
            <?php $images = isset($details['images']) ? $details['images'] : null; ?>
            <?php if ($images && count($images)){ ?>
                <?php foreach ($images as $image){ ?>
                    <a href="<?= SITE.$image['web_path']['xlarge'] ?>" class="swipebox">
                        <img src="<?= SITE.$image['web_path']['large'] ?>" class="img-fluid mx-auto" alt="...">
                    </a>
                <?php } ?>
                <?php } ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="modal-footer">
        <div class="col-md-12 contact-modal">

        </div>
    </div>
    <?php
}
?>
