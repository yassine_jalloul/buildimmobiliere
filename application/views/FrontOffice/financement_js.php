<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script>
    var rates = {
        5: 1.15,
        10: 1.25,
        15: 1.55,
        20: 1.8,
        25: 2,
        30: 2.7
    }

    $(function() {
        setLoanRate();
        setLoanRate2();
        $('form[name="form1"] input[data-change="true"], form[name="form1"] select[data-change="true"]').on('change', function () {
            if($(this).attr("name") == "duration"){
                setLoanRate();
            }
            var l = new Loan($('form[name="form1"] input[name="montant"]').val(),
                $('form[name="form1"] input[name="auto_financement"]').val() || 0,
                $('form[name="form1"] select[name="duration"]').val(),
                $('form[name="form1"] input[name="taux"]').val()
            );
            $('form[name="form1"] .result').hide();
            if(l.monsualyAmount && l.monsualyAmount != NaN){
                $('form[name="form1"] .result .month span').html(l.monsualyAmount + ' ' + l.currency);
                $('form[name="form1"] .result .total span').html(l.totalAmount + ' ' + l.currency);
                $('form[name="form1"] .result .cost span').html(l.cost + ' ' + l.currency);
                $('form[name="form1"] .result .mensuel-revenu span').html(l.monsualyMinimum + ' ' + l.currency);
                $('form[name="form1"] .result').show();
            }
        });

        $('form[name="form2"] input[data-change="true"], form[name="form2"] select[data-change="true"]').on('change', function () {
            if($(this).attr("name") == "duration"){
                setLoanRate2();
            }
            var l = new LoanFromMensualy($('form[name="form2"] input[name="revenue"]').val(),
                0,
                $('form[name="form2"] select[name="duration"]').val(),
                $('form[name="form2"] input[name="taux"]').val()
            );
            $('form[name="form2"] .result').hide();
            if(l.monsualyAmount && l.monsualyAmount != NaN){
                $('form[name="form2"] .result .amount span').html(l.totalAmount + ' ' + l.currency);
                $('form[name="form2"] .result .mensualites span').html(l.monsualyAmount + ' ' + l.currency);
                $('form[name="form2"] .result').show();
            }
        });
    });

    function setLoanRate() {
        var rate = rates[$('form[name="form1"] select[name="duration"]').val()];
        $('form[name="form1"] input[name="taux"]').val(rate);
    }

    function setLoanRate2() {
        var rate = rates[$('form[name="form2"] select[name="duration"]').val()];
        $('form[name="form2"] input[name="taux"]').val(rate);
    }

    function Loan(amount =0, bring=0, duration=0, rate=0) {
        this.amount = amount;
        bring = parseInt(bring);

        this.bring = bring;
        this.totalAllAmount = parseInt(amount, 10) - parseInt(bring, 10)
        this.duration = duration;
        this.durationAll =  parseInt(duration, 10) * 12;
        this.rate = rate;

        this.monsualyAmount = 0;
        this.monsualyMinimum = 0;
        this.totalAmount = 0;
        this.cost = 0;
        this.currency = 'Dinars';
        if(amount && duration){
            this.simulateLoan();
        }
    }
    Loan.prototype.simulateLoan = function() {
        var amount = parseInt(this.amount, 10) - parseInt(this.bring, 10);
        var duration = parseInt(this.duration, 10) * 12;
        var rate = parseFloat(this.rate.replace(",", ".")) / 100;
        var priceByMonth = parseFloat(Number((amount * rate / 12) / (1 - Math.pow(1 + rate / 12, -duration))));
        var minimumMensualy = priceByMonth * (100/40);
        var totalAmount = parseFloat(priceByMonth * this.durationAll)
        this.monsualyAmount = priceByMonth.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        this.totalAmount = totalAmount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        this.cost = parseFloat(totalAmount - this.totalAllAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        this.monsualyMinimum = minimumMensualy.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    };

    function LoanFromMensualy(amount =0, bring=0, duration=0, rate=0) {
        this.revenue = amount;
        this.bring = bring;
        this.duration = duration;
        this.rate = rate;
        var monsualyAmount = (amount) ? (amount * 40 /100).toFixed(2).toLocaleString() : 0;
        var totalAmount = (monsualyAmount) ? (monsualyAmount * 12 * duration).toFixed(2).toLocaleString() : 0;
        this.monsualyAmount = monsualyAmount;
        this.amount = totalAmount;
        this.totalAllAmount = parseInt(totalAmount, 10) - parseInt(bring, 10)
        this.durationAll =  parseInt(duration, 10) * 12;
        this.monsualyMinimum = 0;
        this.totalAmount = 0;
        this.cost = 0;
        this.currency = 'Dinars';
        if(amount && duration){
            this.simulateLoan();
        }

    }
    LoanFromMensualy.prototype.simulateLoan = Loan.prototype.simulateLoan = function() {
        var amount = parseInt(this.amount, 10) - parseInt(this.bring, 10);
        var duration = parseInt(this.duration, 10) * 12;
        var rate = parseFloat(this.rate.replace(",", ".")) / 100;
        var priceByMonth = parseFloat(Number((amount * rate / 12) / (1 - Math.pow(1 + rate / 12, -duration))));
        var minimumMensualy = priceByMonth * (100/40);
        var totalAmount = parseFloat(priceByMonth * this.durationAll)
        this.monsualyAmount = priceByMonth.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        this.totalAmount = totalAmount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        this.cost = parseFloat(totalAmount - this.totalAllAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        this.monsualyMinimum = minimumMensualy.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    };
</script>