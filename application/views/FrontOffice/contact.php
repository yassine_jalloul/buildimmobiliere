<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="map content-page no-padding">
    <div id="map-container" style="height: 400px"></div>
</section>
<div class="container contact-page"> 
    <div class="row">
        <div class="col-md-12 mt-5">
            <h1 class="page-title">Prenez Contact avec nous</h1>
        </div>
        <div class="col-md-8">
            <form id="contactForm" name="contactForm" class="contactForm" method="POST" action="<?= base_url('contact/send_mail') ?>" novalidate>
                <div class="form-group">
                    <label>Nom et prénom</label>
                    <input type="text" name="name" class="form-control" required data-validation-required-message="Ce champ est obligatoire"/>
                    <p class="help-block"></p>
                </div>
                <div class="form-group">
                    <label>Téléphone</label>
                    <input type="tel" name="tel" class="form-control" required data-validation-required-message="Ce champ est obligatoire"/>
                    <p class="help-block"></p>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" required data-validation-required-message="Ce champ est obligatoire"/>
                    <p class="help-block"></p>
                </div>
                <div class="form-group">
                    <label>Sujet</label>
                    <input type="text" name="sujet" class="form-control" required data-validation-required-message="Ce champ est obligatoire"/>
                    <p class="help-block"></p>
                </div>
                <div class="form-group">
                    <label>Message</label>
                    <textarea name="message" required class="form-control" data-validation-required-message="Ce champ est obligatoire"></textarea>
                        <input type="hidden" name="type-form" value="contact-form"/>
                    <p class="help-block"></p>
                </div>
                <div class="form-group">
                    <?= $widget;?>
                    <?= $script;?>
                    <div id="captcha_error"></div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-danger">Envoyer</button>
                </div>
            </form>
        </div>
        <div class="col-md-4 col-lg-4 mt-4">
            <p>
                <i class="fa fa-home mr-3 text-color" title="Adresse"></i>Immobilière Magasin générale<br>28, rue Kamel Attaturk, <br>Bureau 804
            </p>
            <p>
                <i class="fa fa-phone mr-3 text-color" title="Téléphone"></i> +216 71 12 68 00
            </p>
            <p>
                <i class="fa fa-fax mr-3 text-color" title="Fax"></i> +216 71 25 75 02
            </p>
            <p>
                <i class="fa fa-mobile mr-3 text-color" title="Portable"></i> 29 505 234 / 29 929 947
            </p>
        </div>
    </div>
</div>