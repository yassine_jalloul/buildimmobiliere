<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$images = isset($projet['images']) ? $projet['images'] : null;
$count = count($images);
if ($images && count($images) && $count>1):
    ?>
    <div class="swipe-div">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($images as $image): ?>
                    <div class="swiper-slide">
                        <img class="img-fluid" src="<?= SITE . $image['web_path']; ?>">
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div>
<?php endif; ?>
<div class="container <?= $count == 1 ? 'content-page' : 'mt-5' ?>">
    <div class="row">
        <div class="col-md-8 ">
            <h1 class="page-title"><?= $projet['titre']; ?></h1>
            <?php if($count == 1): ?>
                <img src="<?= SITE . $projet['web_path']; ?>" class="img-fluid mt-2">
                <h6 class="text-red mb-2 mt-4 text-uppercase">Description</h6>
            <?php endif; ?>
            <div class="description  mt-2">
                <?= $projet['description']; ?>
            </div>
        </div>
        <div class="col-md-4">
            <?php if ($this->uri->segment(3) == '36'): ?>
                <a href="<?=base_url('visite/mourouj/exterieur')?>" target="_blank">
                    <button class="btn-red text-uppercase mb-3 mt-3">Visite virtuelle</button>
                </a>
            <?php endif; ?>
            <?php if ($this->uri->segment(3) == '37'): ?>
                <a href="<?=base_url('visite/centre-urbain-nord/')?>" target="_blank">
                    <button class="btn-red text-uppercase mb-3 mt-3">Visite virtuelle</button>
                </a>
            <?php endif; ?>
            <?php if ($this->uri->segment(3) == '38'): ?>
                <h2 class="red-title mt-3">Brochure du projet</h2>
                <a href="<?= base_url('sites/default/files/brochure-ennaser.pdf'); ?>">
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure-ennacer.jpg'); ?>">
                </a>
            <?php elseif ($this->uri->segment(3) == '34'): ?>
                <h2 class="red-title mt-3">Brochure du projet</h2>
                <a href="<?= base_url('sites/default/files/brochure-nabeul.pdf'); ?>">
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure-nabeul.jpg'); ?>">
                </a>
            <?php elseif ($this->uri->segment(3) == '37'): ?>
                <h2 class="red-title mt-3">Brochure du projet</h2>
                <a href="<?= base_url('sites/default/files/brochure-centre.pdf'); ?>">
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure-centre.jpg'); ?>">
                </a>
            <?php elseif ($this->uri->segment(3) == '41'): ?>
                <h2 class="red-title mt-3">Brochure du projet</h2>
                <a href="<?= base_url('sites/default/files/brochure-mornag.pdf'); ?>">
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure-mornag.jpg'); ?>">
                </a>
            <?php elseif ($this->uri->segment(3) == '43'): ?>
                <h2 class="red-title mt-3">Brochure du projet</h2>
                <a href="<?= base_url('sites/default/files/brochure-mornaguia.pdf'); ?>">
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure-mornaguia.jpg'); ?>">
                </a>
            <?php elseif($this->uri->segment(3) == '36'): ?>
                <a href="<?= base_url('sites/default/files/brochure.pdf'); ?>">
                    <h2 class="red-title mt-3">Brochure du projet</h2>
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure.jpg'); ?>">
                </a>
            <?php endif; ?>
            <form name="searchForm" method="GET" action="<?= base_url('search') ?>">
                <h2 class="red-title <?= $count == 1 ? 'mt-5' : '' ?>">découvrez nos projets</h2>
                <div class="row pt-3 text-center">
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 mb-2">
                        <select name="property_types[]" class="selectpicker  show-tick" title="Type de bien" data-actions-box="true">
                            <?php foreach ($type_biens as $type_bien): ?>
                                <option value="<?= $type_bien['id']; ?>"><?= $type_bien['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <input type="hidden" name="vocations[]" id="vocation" value="1"/>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 mb-2 div-btn-red">
                        <input type="submit" class="btn-red text-uppercase" name="submit" value="Lancer la recherche">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php if (!empty($properties)): ?>
        <div class="row">
            <div class="col-md-6 mt-5 mb-3">
                <h2 class="red-title pb-3 mt-3">Liste des plans</h2>
            </div>
            <div class="col-md-6 mt-5 mb-3">											
                <div class="ajax-view-icons-container text-right">
                    <?php
                    if ((!empty($_GET)) && ($this->input->get('show') == 'grid')):
                        ?>
                        <a class="view-box" href="<?= $url_search; ?>">
                            <img src="<?= base_url('images/list-icon.png'); ?>" alt=""/>
                        </a>
                        <a href="" class="view-box view-box-active">
                            <img src="<?= base_url('images/grid-icon.png'); ?>" alt=""/>
                        </a>
                    <?php else: ?>
                        <a class="view-box view-box-active" href="">
                            <img src="<?= base_url('images/list-icon.png'); ?>" alt=""/>
                        </a>
                        <a href="" class="view-box">
                            <img src="<?= base_url('images/grid-icon.png'); ?>" alt=""/>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php
        if ($this->uri->segment(3) == '34'):
            ?>
            <div class="row">
                <div class="col-md-12 no-padding">
                    <form class="form-inline plans_search" name="search" method="GET" action="<?= base_url('projets_avenir/search/' . $this->uri->segment(3)); ?>">
                        <div class="col-md-2 mb-3">
                            <h2 class="red-title mt-2">Recherche: </h2>
                        </div>
                        <div class="col-md-5 mb-3">
                            <select name="property_types[]" class="selectpicker show-tick" title="Type de bien" data-actions-box="true">
                                <option selected value="">Type de bien</option>
                                <option value="5">Appartement</option>
                                <option value="12">Commerce</option>
                            </select>
                        </div>
                        <div class="col-md-5 mb-3">
                            <select name="pieces[]" class="selectpicker show-tick" title="Nombre de pièces" data-actions-box="true">
                                <option selected value="">Nombre de pièces</option>
                                <option value="1">S+1</option>
                                <option value="2">S+2</option>
                                <option value="3">S+3</option>
                                <option value="4">S+4</option>
                                <option value="5">S+5</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <?php
        endif;
        ?>
        <div class="row plans">
            <?php
            $this->load->view($properties_items);
            ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-12">
            <h6 class="text-red mb-2 mt-3 text-uppercase">Emplacement</h6>
            <div class="map-container mt-2" id="map-container-projet" style="height: 300px">
                <button class="show-map btn btn-default">Afficher l'emplacement</button>
            </div>
        </div>
    </div>
</div>