<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <!-- External CSS libraries -->
        <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="<?= base_url('images/logo.jpg'); ?>" type="image/x-icon"/>
        <link rel="icon" href="<?= base_url('images/logo.jpg'); ?>" type="image/x-icon"/>
        <link rel="stylesheet" href="<?= base_url('fontawesome/web-fonts-with-css/css/fontawesome.min.css'); ?>">
        <!-- Bootstrap core CSS -->
        <link href="<?= base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?= base_url('plugins/leaflet/leaflet.css'); ?>" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="<?= base_url('css/jquery-ui.min.css'); ?>" rel="stylesheet">
        <link href="<?= base_url('swiper/dist/css/swiper.min.css'); ?>" rel="stylesheet">
        <link href="<?= base_url('css/owl.carousel.min.css'); ?>" rel="stylesheet">
        <link href="<?= base_url('css/owl.theme.default.min.css'); ?>" rel="stylesheet">
        <link href="<?= base_url('swiper/dist/css/swiper.min.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url('bootstrap-select/css/bootstrap-select.css'); ?>">
        <link href="<?= base_url('css/swipebox.min.css'); ?>" rel="stylesheet">
        <link href="<?= base_url('css/style.css?v=1'); ?>" rel="stylesheet">
    </head>
    <body>
        <div class="loader-bg"></div>
        <div id="wrapper">
            <header>
                <?php $this->load->view("FrontOffice/header"); ?>
            </header>
            <main>
                <?php
                $this->load->view($contents);
                ?>
            </main>
            <footer>
                <?php
                $this->load->view("FrontOffice/footer");
                ?>
            </footer>

        </div>
        <script>
            var baseweb = '<?= base_url(); ?>';
        </script>
        <!-- JQuery -->
        <script type="text/javascript" src="<?= base_url('js/jquery-2.1.0.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('js/jquery-ui.min.js'); ?>"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="<?= base_url('js/popper.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="<?= base_url('js/bootstrap.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('js/jqBootstrapValidation.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('js/contact_me.js?v=3'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('fontawesome/svg-with-js/js/fontawesome-all.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('swiper/dist/js/swiper.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('bootstrap-select/js/bootstrap-select.js'); ?>"></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAygQbR3iyxSghoG64SyZKjyUy8sPw93U0&amp;libraries=places"></script>
        <script src="<?= base_url('plugins/leaflet/leaflet.js'); ?>"></script>
        <script src="<?= base_url('js/custom.js'); ?>"></script>
        <?php
        if (!empty($projet)):
            if (!empty($projet['lat']) && !empty($projet['lng'])):
                ?>
                <script type="text/javascript">
                    load_map('map-container-projet', '<?= $projet['titre'] ?>', <?= $projet['lat'] ?>, <?= $projet['lng'] ?>);
                </script>
                <?php
            endif;
        endif;
        ?>
        <script type="text/javascript" src="<?= base_url('js/bootstrap-notify.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('js/owl.carousel.min.js'); ?>"></script>
        <script type="text/javascript">
            $(".owl-carousel").owlCarousel({
                loop: $(this).find('.owl-carousel .item').size() > 1 ? true : false,
                margin: 10,
                nav: true,
                navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            });
        </script>
        <script type="text/javascript" src="<?= base_url('js/jquery.swipebox.min.js'); ?>"></script>
        <script>
            var url_list_plans = '<?= base_url('projets_avenir/detail/' . $this->uri->segment(3)); ?>'
        </script>
        <script type="text/javascript">
                    ;
            (function ($) {

                $('.swipebox').swipebox();

            })(jQuery);

            (function () {
                /*$('form[name="form1"] input[data-change="true"]').on('change', function () {
                    var form = $($(this).parents('form')[0]);
                    var formData = form.serializeArray();
                    var tab = {};
                    var sum = 0;
                    formData.forEach(function (elem) {
                        var name = elem.name;
                        var value = elem.value;
                        if (name != "result" && value && parseInt(value)) {
                            tab[name] = parseInt(value);
                            sum += parseInt(value);
                        }
                    })
                    //console.dir(tab);
                    if('montant' in tab && 'period' in tab && 'taux' in tab){
                        if(tab.montant && tab.period && tab.taux){
                            var mensualite = (tab.montant*(tab.taux/100)/12)/(1-Math.pow((1+(tab.taux/100)/12),-(tab.period*12)))

                        }
                    }
                    if (Object.keys(tab).length == form.find('input[data-change="true"]').length) {
                        form.find('input[name="result"]').val(Math.round(mensualite*100)/100);
                    } else {
                        form.find('input[name="result"]').val('');
                    }

                });

                $('form[name="form2"] input[data-change="true"]').on('change', function () {
                    var form = $($(this).parents('form')[0]);
                    var formData = form.serializeArray();
                    var tab = {};
                    var sum = 0;
                    formData.forEach(function (elem) {
                        var name = elem.name;
                        var value = elem.value;
                        if (name != "montant1" && name != "mensualites" && value && parseInt(value)) {
                            tab[name] = parseInt(value);
                            sum += parseInt(value);
                        }
                    })
                    if (Object.keys(tab).length == form.find('input[data-change="true"]').length) {
                        form.find('input[name="montant1"]').val(sum);
                        form.find('input[name="mensualites"]').val(sum);
                    } else {
                        form.find('input[name="montant1"]').val('');
                        form.find('input[name="mensualites"]').val(sum);
                    }

                });*/
            })();
            $(function () {
                makeHrefAjaxRequest();

                $('form.plans_search .selectpicker').on('change', function () {
                    //var selected = $(this).find("option:selected").val();
                    getPlans($('form.plans_search').attr('action'));
                });
                $('.ajax-view-icons-container .view-box').click(function () {
                    $('.ajax-view-icons-container .view-box').removeClass('view-box-active')
                    $(this).addClass('view-box-active');
                    var href = url_list_plans;
                    var page = $('.plans .item-list .pager li.pager-current').html()
                    page = parseInt(page);
                    if (page) {
                        href += '/' + page;
                    }
                    getPlans(href);
                    return false;
                })
            })
            function makeHrefAjaxRequest() {
                $('.plans .item-list .pager li a').click(function () {
                    var href = $(this).attr('href');
                    getPlans(href);
                    return false;
                });
            }
            function getPlans(url) {
                var data = getDataPlansToSearch();
                showLoaderPlans();
                $.ajax({
                    url: url,
                    data: data,
                    type: 'GET',
                    success: function (result) {
                        $('.plans').html(result);
                        makeHrefAjaxRequest();
                        removeLoaderPlans()
                    }
                }).fail(function () {
                    removeLoaderPlans();
                })
            }

            function getDataPlansToSearch() {
                var dataArray = $('form.plans_search').serializeArray();
                var dataPlans = [];
                dataArray.forEach(function (e) {
                    var name = e.name;
                    var value = e.value;
                    if (value) {
                        dataPlans.push(name + '=' + value);
                    }
                });
                var data = dataPlans.join('&');
                var show_mode = $('.ajax-view-icons-container .view-box-active').index()
                var query = '';
                if (show_mode) {
                    query = 'show=grid';
                } else {
                    query = '';
                }
                if (data && query) {
                    data += '&' + query;
                } else {
                    data += query;
                }
                return data;
            }

            function showLoaderPlans() {
                if (!$('.plans .overlay-plan').length) {
                    $('.plans').append('<div class="overlay-plan"><div class="loader"></div></div>')
                }
            }

            function removeLoaderPlans() {
                $('.plans .overlay-plan').remove();
            }

            function submitModal(modal_id) {
                $("#" + modal_id + " .myButton1").click();
                //$('#' + modal_id).modal('toggle');
            }
        </script>
        <script type="text/javascript" src="<?= base_url('js/main.js?v=0'); ?>"></script>

        <?php isset($contents_js) ? $this->load->view($contents_js) : "" ?>
    </body>
</html>
