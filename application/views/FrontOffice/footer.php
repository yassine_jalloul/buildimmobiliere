<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!--Footer-->
<footer class="footer page-footer mt-100 text-center">
    <?php if ($this->uri->segment(1) != 'contact'): ?>
        <!--Footer Links-->
        <div class="container mb-4 text-center text-md-left">
            <div class="row mt-3">
                <div class="col-md-4 col-lg-4 col-xl-3 mx-auto">
                    <h6 class="text-red mb-2 text-uppercase">
                        <strong>Prenez Contact avec nous</strong>
                    </h6>
                    <p>
                        <i class="fa fa-home mr-3" title="Adresse"></i>Immobilière Magasin générale<br>28, rue Kamel Attaturk, <br>Bureau 804</p>
                    <p>
                        <i class="fa fa-phone mr-3" title="Téléphone"></i> +216 71 12 68 00</p>
                    <p>
                        <i class="fa fa-fax mr-3" title="Fax"></i> +216 71 25 75 02</p>
                    <p>
                        <i class="fa fa-mobile mr-3" title="Portable"></i> 29 505 234 / 29 929 947</p>
                </div>
                <!--/.First column-->

                <!--Second column-->
                <div class="col-md-4 col-lg-4 col-xl-4 mx-auto back-grey p-3">                   
                    <form id="contactForm" name="contact" class="contactForm" method="POST" class="form-vertical">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control mb-3 border-radius-0" required placeholder="Nom et prénom*" data-validation-required-message="Ce champ est obligatoire">
                            <p class="help-block"></p>
                        </div>
                        <div class="form-group">
                            <input type="tel" name="tel" class="form-control mb-3 border-radius-0" required placeholder="Téléphone" data-validation-required-message="Ce champ est obligatoire">
                            <p class="help-block"></p>
                        </div>
                        <div class="form-group">
                            <input type="mail" name="email" class="form-control mb-3 border-radius-0" required placeholder="E-mail*" data-validation-required-message="Ce champ est obligatoire">
                            <p class="help-block"></p>
                        </div>
                        <div class="form-group">
                            <input type="text" name="sujet" class="form-control mb-3 border-radius-0" required placeholder="Sujet*" data-validation-required-message="Ce champ est obligatoire">
                            <p class="help-block"></p>
                        </div>
                        <div class="form-group">
                            <textarea name="message" class="form-control mb-3 border-radius-0" required placeholder="Votre Message*" data-validation-required-message="Ce champ est obligatoire"></textarea>
                            <p class="help-block"></p>
                        </div>
                        <div class="form-group">
                            <?= $widget;?>
                            <?= $script;?>
                            <div id="captcha_error"></div>
                        </div>
                        <input type="hidden" name="type-form" value="contact-form"/>
                        <div class="form-group">
                            <input type="submit" name="send" value="Envoyer" class="btn-envoyer">
                        </div>
                    </form>
                </div>
                <div class="col-md-4 col-lg-4 col-xl-4 mx-auto">                 
                    <div id="map-container" style="height: 360px"></div>

                </div>
                <!--/.Fourth column-->

            </div>
        </div>
        <!--/.Footer Links-->
    <?php endif; ?>
    <!--Copyright-->
    <div class="footer-copyright py-2 back-red">
        <div class="container-fluid text-center text-uppercase">
            © Build Immobilière <?= date('Y', time()); ?> Conçu avec <a target="_blank" href="http://immotech.tn/">iMMOTECH</a> par <a target="_blank" href="http://conceptlab.tn/">ConceptLab</a>

        </div>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->
