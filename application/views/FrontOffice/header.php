<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$futur = array();
$en_cours = array();
$realise = array();
if (!empty($projets)):
    foreach ($projets as $projet):
        if ($projet['statut']['name'] == 'Futur'):
            $futur[] = $projet;
        elseif ($projet['statut']['name'] == 'En cours'):
            $en_cours[] = $projet;
        else:
            $realise[] = $projet;
        endif;
    endforeach;
endif;
?>
<nav class="navbar navbar-expand-lg navbar-light fixed-top scrolling-navbar top-nav-collapse">
    <div class="container">
        <!-- Navbar brand -->
        <a class="navbar-brand" href="<?= base_url(); ?>">
            <img src="<?= base_url('images/logo.jpg'); ?>">
        </a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Links -->
            <ul class="navbar-nav mx-auto">
                <li class="nav-item <?= $this->uri->segment(1) == '' || $this->uri->segment(1) == 'search' ? 'active' : ''; ?>">
                    <a class="nav-link" href="<?= base_url(); ?>">Accueil <span class="sr-only">(current)</span></a>
                </li>
                <!-- Dropdown -->
                <?php if (!empty($en_cours)): ?>
                    <li class="nav-item dropdown <?= $this->uri->segment(1) == 'projets_en_cours' ? 'active' : ''; ?>">
                        <a class="nav-link dropdown-toggle" id="projets_venir" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Projets en cours
                        </a>
                        <div class="dropdown-menu dropdown-primary" aria-labelledby="projets_en_cours">
                            <?php foreach ($en_cours as $row): ?>
                                <a class="dropdown-item my-2" href="<?= base_url('projets_en_cours/detail/' . $row['id']); ?>">
                                    <?= $row['titre'] ?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </li>
                <?php endif; ?>
                <?php if (!empty($realise)): ?>
                    <li class="nav-item dropdown <?= $this->uri->segment(1) == 'projets_realises' ? 'active' : ''; ?>">
                        <a class="nav-link dropdown-toggle" id="projets_venir" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Projets réalisés
                        </a>
                        <div class="dropdown-menu dropdown-primary" aria-labelledby="projets_realises">
                            <?php foreach ($realise as $row): ?>
                                <a class="dropdown-item my-2" href="<?= base_url('projets_realises/detail/' . $row['id']); ?>">
                                    <?= $row['titre'] ?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </li>
                <?php endif; ?>
                <?php if (!empty($futur)): ?>
                    <li class="nav-item dropdown <?= $this->uri->segment(1) == 'projets_avenir' ? 'active' : ''; ?>">
                        <a class="nav-link dropdown-toggle" id="projets_venir" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Projets à venir
                        </a>
                        <div class="dropdown-menu dropdown-primary" aria-labelledby="Projets_avenir">
                            <?php foreach ($futur as $row): ?>
                                <a class="dropdown-item my-2" href="<?= base_url('projets_avenir/detail/' . $row['id']); ?>">
                                    <?= $row['titre'] ?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </li>
                <?php endif; ?>
                <li class="nav-item <?= $this->uri->segment(1) == 'contact' ? 'active' : ''; ?>">
                    <a class="nav-link" href="<?= base_url('contact'); ?>">Contact</a>
                </li>
                <li class="nav-item <?= $this->uri->segment(1) == 'financement' ? 'active' : ''; ?>">
                    <a class="nav-link" href="<?= base_url('financement'); ?>">Financement</a>
                </li>
            </ul>
            <!-- Links -->
        </div>
        <!-- Collapsible content -->
    </div>
</nav>
