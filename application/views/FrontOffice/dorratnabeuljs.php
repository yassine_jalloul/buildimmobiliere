<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$version = '0.0.0';
?>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script>
    $(document).ready(function(){
        appendVisite();
        removeVisite();
		$('#datetimepicker1').datetimepicker({
			//disabledHours: [20, 21, 22, 23, 24, 1, 2, 3, 4, 5, 6]
			locale: 'fr',
			inline: true,
			sideBySide: true,
			format: "YYYY/MM/DD HH:mm"
		});
		$('#datetimepicker1').on('dp.change', function(e) {
			config3D.changePositionOfLight(e.date.toDate());
		})
    })
	var width = $(window).width();
</script>

<script>
	var is_mobile = <?= $isMobile ? 1 : 0 ?>;
</script>
<script>
	var properties = <?=count($propertiesDrawList) ? json_encode($propertiesDrawList) : json_encode([]) ?>;
	var obj3D = 'mg.obj?v=<?=$version?>';
	var objMtl3D = 'mg.mtl?v=<?=$version?>';
	var objDir3D = '<?= base_url('plugins/dorratnabeul3d/dist/assets/3d/') ?>';
	var backgroundSound = '<?= base_url('plugins/dorratnabeul3d/dist/3d/sounds/darktimes.mp3') ?>';
	var clickSound = '<?= base_url('plugins/dorratnabeul3d/dist/3d/sounds/click.mp3') ?>';
	var textures = {
		'texture_1':'<?= base_url('plugins/dorratnabeul3d/dist/assets/3d/maps/texture1.png') ?>?v=<?=$version?>',
		'texture_2':'<?= base_url('plugins/dorratnabeul3d/dist/assets/3d/maps/texture2.jpg') ?>?v=<?=$version?>',
		'texture_3':'<?= base_url('plugins/dorratnabeul3d/dist/assets/3d/maps/texture3.jpg') ?>?v=<?=$version?>',
		'texture_4':'<?= base_url('plugins/dorratnabeul3d/dist/assets/3d/maps/texture4.jpg') ?>?v=<?=$version?>',
		'texture_5':'<?= base_url('plugins/dorratnabeul3d/dist/assets/3d/maps/texture5.jpg') ?>?v=<?=$version?>',
		'texture_6':'<?= base_url('plugins/dorratnabeul3d/dist/assets/3d/maps/texture6.jpg') ?>?v=<?=$version?>',
		'texture_7':'<?= base_url('plugins/dorratnabeul3d/dist/assets/3d/maps/texture7.jpg') ?>?v=<?=$version?>',
		'texture_10':'<?= base_url('plugins/dorratnabeul3d/dist/assets/3d/maps/texture10.jpg') ?>?v=<?=$version?>',
		'texture_20':'<?= base_url('plugins/dorratnabeul3d/dist/assets/3d/maps/texture20.png') ?>?v=<?=$version?>',
	}

    function appendVisite(){
        $('#visite360').on('show.bs.modal', function (e) {
            $('#visite360 .modal-body').html('<iframe width="100%" height="500" src="'+baseweb+'visite/chaabaneap1/" frameborder="0" allowfullscreen></iframe>')
        })
    }
    function removeVisite() {
        $('#visite360').on('hidden.bs.modal', function (e) {
            $('#visite360 .modal-body iframe').remove();
        })

    }
</script>

<script type="text/javascript" src="<?= base_url('plugins/dorratnabeul3d/dist/build.js') ?>?v=<?=$version?>"></script>
<script type="text/javascript" src="<?= base_url('js/dorratnabeul3d.js'); ?>?v=<?=$version?>"></script>
