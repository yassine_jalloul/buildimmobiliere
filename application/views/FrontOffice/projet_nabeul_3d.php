<div class="content-column  col-lg-12 col-md-12 col-sm-12">
    <div class="">
        <h4>Maquette 3D Interactive de la Résidence DORRAT NABEUL </h4>
    </div>
</div>
<div id="full-container-3d" class="content-column auto-container p-xs-0" style="margin-bottom: 3rem">
    <div id="ui-tooltip-custom" class="ui-tooltip-custom"></div>
    <div class="modal right fade" id="showModalDetail3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"  data-backdrop="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Villa mercure</h4>
                </div>
                <div class="modal-body">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
                <div class="modal-footer">
                    <a href="#" class="detail-property-link btn btn-primary">Voir plus de details</a>
                    <a href="<?= base_url('/') ?>" class="btn btn-default">contact</a>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <div class="modal fade" id="searchModal3D" tabindex="-1" role="dialog" data-backdrop="false">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">

            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <div class="modal fade" id="settingModal3D" tabindex="-1" role="dialog" data-backdrop="false">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel">Settings</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="shadow-3d">Shadow</label>
                            <input id="shadow-3d" type="checkbox" onChange="config3D.toggleShadow()" name='shadow' checked='checked'/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Fermer</button>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <div class="modal right fade" id="timeModal3D" tabindex="-1" role="dialog" data-backdrop="false">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content" style="background-color: rgba(251, 247, 247, 0.9);border: 1px solid;">
                <form>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="myModalLabel">Date</h5>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="datetimepicker1"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Fermer</button>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
    <div class="modal fade" id="visite360" tabindex="-1" role="dialog" aria-labelledby="visite360" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Visite 360°</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <div class=" position-relative">
        <div class="row">
            <div class="col-md-12">
                <i class="fa fa-search icon-search" id="icon-search" onclick="ShowHideSearchForm()"></i>
                <div class="SearchFormDiv">
                    <form action="" method="get" class="SearchFormDetail" onsubmit="return false;">
                        <div class="form-group clearfix">
                            <i class="fa fa-times closesearch pull-right" onclick="ShowHideSearchForm()"></i>
                        </div>
                        <div class="form-group">
                            <select name="etage" class="selectpicker search-fields" style="width: 100%">
                                <option value="-2">Étage</option>
                                <option value="-1">RDJ</option>
                                <option value="0">RDC</option>
                                <option value="1">Étage 1</option>
                                <option value="2">Étage 2</option>
                                <option value="3">Étage 3</option>
                                <option value="4">Étage 4</option>
                                <option value="5">Étage 5</option>
                                <option value="6">Étage 6</option>
                                <option value="7">Étage 7</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="pieces" class="selectpicker search-fields" style="width: 100%">
                                <option value="">Pièces</option>
                                <option value="1">S + 1</option>
                                <option value="2">S + 2</option>
                                <option value="3">S + 3</option>
                                <option value="4">S + 4</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="surface" class="selectpicker search-fields" style="width: 100%">
                                <option value="">Surface (m²)</option>
                                <option value="50_100">50 - 100</option>
                                <option value="100_200">100 - 150</option>
                                <option value="150_200">150 - 200</option>
                                <option value="200_250">200 - 250</option>
                                <option value="250_300">250 - 300</option>
                                <option value="300_350">300 - 350</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="input-text refernce-input" name="reference" value=""
                                   placeholder="Référence" type="text" style="">
                        </div>
                        <div>
                            <button type="reset" class="theme-btn btn-style-three" onclick="resetFromSearch();">Réinitialiser</button>
                        </div>
                    </form>
                </div>
                <div class="button-toolbar" style="display: none">
                    <button class="button-3d" onclick="resetFromSearch();"><i class="fa fa-home" aria-hidden="true"></i>
                    </button>
                    <button class="button-3d" onclick="goFullScreen()"><i class="fa fa-arrows-alt" aria-hidden="true"></i>
                    </button>
                    <button class="button-3d" onclick="playScene(this)"><i class="fa fa-play" aria-hidden="true"></i></button>
                    <button class="button-3d" onclick="openSettingModal()"><i class="fa fa-sliders" aria-hidden="true"></i>
                    </button>
                    <button class="button-3d" onclick="toggleSound(this);"><i class="fa fa-volume-off"></i></button>
                    <button class="button-3d" onclick="toggleCompass(this);"><i class="fa fa-compass"></i></button>
                    <button class="button-3d" onclick="openTimeModal(this);"><i class="fa fa-clock-o"></i></button>
                </div>
                <div class="before-load">
                    <button class="theme-btn btn-style-one text-uppercase" onclick="load_3d()">Explorer le projet</button>
                    <img src="<?= base_url('assets/images/D.jpg') ?>" class="img-fluid" alt="RAYAN" title="RAYAN">
                </div>
                <div class="" id="root" style="height: 100vh;display:none"></div>
            </div>
        </div>
    </div>
</div>