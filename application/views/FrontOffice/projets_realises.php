<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$images = isset($projet['images']) ? $projet['images'] : null;
if ($images && count($images)):
    ?>
    <div class="swipe-div">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($images as $image): ?>
                    <div class="swiper-slide">
                        <img class="img-fluid" src="<?= SITE . $image['web_path']; ?>">
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div>
<?php endif; ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5 mytabs">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active js-scroll-trigger" href="#presentation">Presentation</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#plan_de_situation">Plan de situation</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#equipements">Equipements</a>
                </li>

            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 pt-3">
            <div class="description  mt-2" id="presentation">
                <h2 class="red-title pb-3"><?= $projet['titre']; ?></h2>
                <?= $projet['description']; ?>
            </div>
        </div>
        <div class="col-md-4">
            <?php if ($this->uri->segment(3) == '36'): ?>
                <a href="<?=base_url('visite/mourouj/exterieur')?>" target="_blank">
                    <button class="btn-red text-uppercase mb-3 mt-3">Visite virtuelle</button>
                </a>
            <?php endif; ?>
            <?php if ($this->uri->segment(3) == '37'): ?>
                <a href="<?=base_url('visite/centre-urbain-nord/')?>" target="_blank">
                    <button class="btn-red text-uppercase mb-3 mt-3">Visite virtuelle</button>
                </a>
            <?php endif; ?>
            <?php if ($this->uri->segment(3) == '38'): ?>
                <h2 class="red-title mt-3">Brochure du projet</h2>
                <a href="<?= base_url('sites/default/files/brochure-ennaser.pdf'); ?>">
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure-ennacer.jpg'); ?>">
                </a>
            <?php elseif ($this->uri->segment(3) == '34'): ?>
                <h2 class="red-title mt-3">Brochure du projet</h2>
                <a href="<?= base_url('sites/default/files/brochure-nabeul.pdf'); ?>">
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure-nabeul.jpg'); ?>">
                </a>
            <?php elseif ($this->uri->segment(3) == '37'): ?>
                <h2 class="red-title mt-3">Brochure du projet</h2>
                <a href="<?= base_url('sites/default/files/brochure-centre.pdf'); ?>">
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure-centre.jpg'); ?>">
                </a>
            <?php elseif ($this->uri->segment(3) == '41'): ?>
                <h2 class="red-title mt-3">Brochure du projet</h2>
                <a href="<?= base_url('sites/default/files/brochure-mornag.pdf'); ?>">
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure-mornag.jpg'); ?>">
                </a>
            <?php elseif ($this->uri->segment(3) == '43'): ?>
                <h2 class="red-title mt-3">Brochure du projet</h2>
                <a href="<?= base_url('sites/default/files/brochure-mornaguia.pdf'); ?>">
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure-mornaguia.jpg'); ?>">
                </a>
            <?php elseif($this->uri->segment(3) == '36'): ?>
                <a href="<?= base_url('sites/default/files/brochure.pdf'); ?>">
                    <h2 class="red-title mt-3">Brochure du projet</h2>
                    <img class="img-fluid width-100 mt-3" src="<?= base_url('images/brochure.jpg'); ?>">
                </a>
            <?php endif; ?>
            <form name="searchForm" method="GET" action="<?= base_url('search') ?>">
                <h2 class="red-title mt-3">découvrez nos projets</h2>
                <div class="row pt-3 text-center">
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 mb-2">
                        <select name="property_types[]" class="selectpicker  show-tick" title="Type de bien" data-actions-box="true">
                            <?php foreach ($type_biens as $type_bien): ?>
                                <option value="<?= $type_bien['id']; ?>"><?= $type_bien['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <input type="hidden" name="vocations[]" id="vocation" value="1"/>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 mb-2 div-btn-red">
                        <input type="submit" class="btn-red text-uppercase" name="submit" value="Lancer la recherche">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row plan_situation" id="plan_de_situation">
        <div class="col-md-7">
            <div class="map-container" id="map-container-projet" style="height: 250px;"></div>
        </div>
    </div>
    <?php if (!empty($properties)): ?>
        <div class="row">
            <div class="col-md-6 mt-5 mb-3">
                <h2 class="red-title pb-3 mt-3">Liste des plans</h2>
            </div>
            <div class="col-md-6 mt-5 mb-3">
                <div class="ajax-view-icons-container text-right">
                    <?php
                    if ((!empty($_GET)) && ($this->input->get('show') == 'grid')):
                        ?>
                        <a class="view-box" href="<?= $url_search; ?>">
                            <img src="<?= base_url('images/list-icon.png'); ?>" alt=""/>
                        </a>
                        <a href="" class="view-box view-box-active">
                            <img src="<?= base_url('images/grid-icon.png'); ?>" alt=""/>
                        </a>
                    <?php else: ?>
                        <a class="view-box view-box-active" href="">
                            <img src="<?= base_url('images/list-icon.png'); ?>" alt=""/>
                        </a>
                        <a href="" class="view-box">
                            <img src="<?= base_url('images/grid-icon.png'); ?>" alt=""/>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php
        if ($this->uri->segment(3) == '36' || $this->uri->segment(3) == '34'):
            ?>
            <div class="row">
                <div class="col-md-12 no-padding">
                    <form class="form-inline plans_search" name="search" method="GET" action="<?= base_url('projets_avenir/search/' . $this->uri->segment(3)); ?>">
                        <div class="col-md-3 mb-3">
                            <h2 class="red-title mt-2">Recherche: </h2>
                        </div>
                        <div class="col-md-3 mb-3">
                            <select name="property_types[]" class="selectpicker show-tick" title="Type de bien" data-actions-box="true">
                                <option selected value="">Type de bien</option>
                                <option value="5">Appartement</option>
                                <option value="12">Commerce</option>
                            </select>
                        </div>
                        <div class="col-md-3 mb-3">
                            <select name="pieces[]" class="selectpicker show-tick" title="Nombre de pièces" data-actions-box="true">
                                <option selected value="">Nombre de pièces</option>
                                <option value="1">S+1</option>
                                <option value="2">S+2</option>
                                <option value="3">S+3</option>
                                <option value="4">S+4</option>
                            </select>
                        </div>
                        <div class="col-md-3 mb-3">
                            <select name="etage[]" id="etage" class="selectpicker show-tick" title="Étage" data-actions-box="true">
                                <option selected value="">Étage</option>
                                <option value="0">RDC</option>
                                <option value="1">Étage 1</option>
                                <option value="2">Étage 2</option>
                                <option value="3">Étage 3</option>
                                <option value="4">Étage 4</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <?php
        endif;
        ?>
        <div class="row plans">
            <?php
            $this->load->view($properties_items);
            ?>
        </div>
    <?php endif; ?>
    <?php if ($this->uri->segment(3) == '36'): ?>
        <div class="row mt-3">
            <div class="col-md-12">
                <h2 class="red-title mt-2">Plan étage: </h2>
            </div>
            <div class="col-md-6">
                <a href="<?= base_url('images/plan-r+1-large.jpg'); ?>" class="swipebox">
                    <img src="<?= base_url('images/plan-r+1-small.jpg'); ?>" class="img-fluid"/>
                </a>
            </div>
            <div class="col-md-6">
                <a href="<?= base_url('images/plan-etage-2-large.jpg'); ?>" class="swipebox">
                    <img src="<?= base_url('images/plan-etage-2-small.jpg'); ?>" class="img-fluid"/>
                </a>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($this->uri->segment(3) == '34'): ?>
        <div class="row equipements" id="equipements">
            <div class="field-label">Equipements:</div>
            <p>
                Le gabarit du projet essaie de répondre à la rue structurante par une façade alignée, tout en offrant une cour intérieure importante qui structure les appartements et confère au projet une protection et un aspect.
            </p>
            <p>
                Aux étages, les appartements sont de haut standing variés par leurs aspects, et offrant des façades aux lectures diverses tout en essayant d’harmoniser cette diversité pour en faire une unité. Et offrent les aspects techniques suivants :
            </p>
            <ul>
                <li>Des matériaux de construction de premier choix garantissant la solidité et la pérennité de l'ensemble.</li>
                <li>Des parois extérieures et intérieures avec isolation thermique et phonique.</li>
                <li> Finitions avec enduit en plâtre assurant qualité, aspect, régularité et offrant des performances thermiques, acoustiques et sécuritaires supérieures.</li>
                <li>Revêtements en marbre blanc GIOIA de premier choix dans les couloirs et les salons, marbre écru THALA BEIGE SELEX pour les chambres ; grès cérame de premier choix dans les cuisines, les salles de bains, les salles d'eau et les séchoirs.</li>
                <li>Porte principale en bois noble "frêne massif" de premier choix.</li>
                <li>Portes intérieures iso planes de 35mm, avec un double parement de panneaux aux placages naturels de frêne, finition noyé chêne clair, le bâti en massif de frêne.</li>
                <li>Dressings en panneaux aménagés et équipés avec caisson et étagères. Leurs portes avec placage naturel de frêne.</li>
                <li>Quincaillerie de premier choix.</li>
                <li>Menuiserie extérieure en aluminium, iso phonique et iso thermique assurée par les profilés d’aluminium TPR série PRESTIGE et ELIPSE avec âme pleine en double vitrage.</li>
                <li>Volets roulants électrique avec lames remplies de mousse polyuréthane injectée.</li>
                <li>Grilles de ventilation en profilés d’aluminium TPR pour les séchoirs.</li>
                <li>Gardes corps en aluminium avec vitrage et main courante.</li>
                <li>Cuisines modernes et bien agencées de très haut standing avec des équipements électroménagers.</li>
                <li>Equipements électriques haut de gamme.</li>
                <li>Faux plafonds en plâtre lisse avec des moulures décoratives et des spots encastrés pour les couloirs et halls.</li>
                <li>Peinture à l'eau avec mastic sur support en plâtre et peinture décorative (Stucco) pour les couloirs en communs.</li>
                <li>Système d'alarme et portes télécommandées au sous-sol.</li>
                <li>Interphone et Télédistribution numérique.</li>
                <li>Sanitaires et robinetterie haut de gamme.</li>
                <li>Chauffage central et chaudière installées.</li>
                <li>Climatisation centrale générale pour toutes les chambres et les salons par unités de climatisation multi-split système mural, chaud et froid.</li>
                <li>Systèmes de désenfumage, de ventilation mécanique contrôlée, d'extraction au sous-sol, dans les parties communes et dans les sanitaires.</li>
            </ul>
        </div>
    <?php endif; ?>
    <?php if ($this->uri->segment(3) == '36'): ?>
        <div class="row equipements" id="equipements">
            <div class="field-label">Equipements:</div>
            <p>
                Afin d'assurer le confort et le bien être des résidents, "TEJ ELMOUROUJ" propose différents types d'appartements, S+1/S+2/ S+3 et S+4, de très haut standing offrant les prestations suivantes:
            </p>
            <ul>
                <li>Des matériaux de construction de premier choix garantissant la solidité et la pérennité de l'ensemble.</li>
                <li>Double cloison avec isolation thermique et phonique.</li>
                <li>Finitions avec enduit en plâtre assurant qualité, aspect, régularité et offrant des performances thermiques, acoustiques et sécuritaires supérieures.</li>
                <li>Quatre blocs de circulation verticales composés d'un escalier revêtu en marbre et d'un ascenseur luxueux.</li>
                <li>Revêtements en marbre de premier choix.</li>
                <li>Revêtements en grès cérame de premier choix dans les cuisines, les salles de bains, les salles d'eau, les terrasses et les séchoirs.</li>
                <li>Porte en bois noble "frêne massif" de premier choix.</li>
                <li>Dressings en MDF aménagés et équipés.</li>
                <li>Quincaillerie de premier choix.</li>
                <li>Menuiserie extérieure en aluminium avec double vitrage.</li>
                <li>Volets roulants électriques avec mousse polyuréthane injectée.</li>
                <li>Grilles de ventilation en aluminium pour les séchoirs.</li>
                <li>Gardes corps en aluminium avec vitrage.</li>
                <li>Cuisines agencées de très haut standing avec des équipements électroménagers en option.</li>
                <li>Equipements électriques haut de gamme.</li>
                <li>Faux plafonds en plâtre lisse avec des moulures décoratives et des spots encastrés.</li>
                <li>Peinture à l'eau avec mastic sur support en plâtre.</li>
                <li>Système d'alarme et portes télécommandées au sous-sol.</li>
                <li>Interphone.</li>
                <li>Télédistribution numérique.</li>
                <li>Sanitaires et robinetterie haut de gamme.</li>
                <li>Chauffage central.</li>
                <li>Climatisation générale par split système en option.</li>
                <li>Systèmes de désenfumage, de ventilation mécanique contrôlée et d'extraction au sous-sol, dans les parties communes et dans les sanitaires.</li>
            </ul>
            <p>
                "TEJ ELMOUROUJ" vous offre la possibilité de personnaliser votre logement en y apportant des modifications sur les travaux de finitions qui n'ont pas été exécutés.
            </p>
        </div>
    <?php endif; ?>
    <?php if ($this->uri->segment(3) == '37'): ?>
        <div class="row equipements" id="equipements">
            <div class="field-label">Equipements:</div>
            <ul>
                <li>Toutes les zones limitrophes du bâtiment ainsi que les façades du site avoisinant, sont équipées  par des caméras de surveillance. </li>
                <li>L’éclairage des abords est constitué de balises indiquant les chemins d’accès au site. </li>
                <li>Quelques accessoires lumineux seront particulièrement utilisés pour un éclairage scénographique permettant de signaler les éléments qui mettent en valeur les traits d’expressions du projet. </li>
                <li>Les garde-corps des abords et zones communes sont réalisés en structure d’acier galvanisé fixée au gros œuvre de type claire voie (en maille de profilés métalliques galvanisés) afin de transparaître les espaces extérieures du projet. Les mains courantes sont en acier galvanisé. </li>
                <li>Afin de garantir à tout moment un aspect acceptable  des jardins, l’entretien de la partie commune et des abords est à la charge du syndic des copropriétaires- Le bâtiment est composé de 2 sous-sols, un rez de chaussée et 11 étages, chaque étage  contient quatre compartiments communiquant  par des noyaux centraux de circulation (ascenseurs, escaliers, couloirs), le nombre total des  compartiments est de 48. </li>
                <li>Le bâtiment possède 2 halls d’entrée, 8 ascenseurs et 4 escaliers de secours.</li>
            </ul>
            <p>Quelques Surfaces :</p>
            <ul>
                <li>La surface totale nette des compartiments est de 15 600 m² (sans les surfaces communes).</li>
                <li>La surface d’un étage (4 compartiments) est comprise entre 1 350et 1 900 m². </li>
                <li>La Surface d’un compartiment est comprise entre 174 et 465 m² (surface utile)</li>
                <li>2 halls d’entrée : grand patio :     190  m² et  petit patio :   50  m²</li>
            </ul>
        </div>
    <?php endif; ?>
    <?php if ($this->uri->segment(3) == '38'): ?>
        <div class="row equipements" id="equipements">
            <div class="field-label">Equipements:</div>
            <p>
                En plus du parking, le centre contient un espace commercial et de loisirs de trois étages offrant un magasin de proximité, un étage commercial au RDC 2 et une aire de loisirs et de restauration au 8ème étage permettant une vue exceptionnelle.
            </p>
            <p>
                Le centre est équipé de 2 ascenseurs d’une capacité de 12 personnes chacun menant à tous les étages.
            </p>
            <p>
                Un écran géant numérique sera place sur la façade principale du projet pour servir à la publicité et à la promotion du centre.
            </p>
        </div>
    <?php endif; ?>
</div>