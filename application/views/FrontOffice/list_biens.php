<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!empty($properties)):
    ?>
    <div class="col-md-12">
        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th>Numéro</th>
                    <th>&Eacute;tage</th>
                    <th>Type</th>
                    <th>Pièces</th>
                    <th>Surface</th>
                    <th>Plans</th>
                    <th>Demande de devis</th>
                    <?php if ($this->uri->segment(3) == '36'||$this->uri->segment(3) == '37'): ?>
                        <th>Visite virtuelle</th>
                    <?php endif; ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($properties as $propertie): ?>
                    <tr>
                        <td data-title="Numéro">
                            <?= $propertie['name']; ?>
                        </td>
                        <td data-title="&Eacute;tage">
                            <?php if ($propertie['etage'] >= 1): ?>
                                Étage <?= $propertie['etage']; ?>
                            <?php elseif ($propertie['etage'] == 0): ?>
                                RDC
                            <?php elseif ($propertie['etage'] == -1): ?>
                                Sous sol
                            <?php endif; ?>
                        </td>
                        <td data-title="Type"><?= $propertie['property_type']['id'] == 13 ? 'Tous commerces' : $propertie['property_type']['name']; ?></td>
                        <?php if (!empty($propertie['total_pieces'])): ?>
                            <td data-title="Pièces">S+<?= $propertie['total_pieces']; ?></td>
                        <?php else: ?>
                            <td data-title="Pièces"></td>
                        <?php endif; ?>
                        <td data-title="Surface"><?= !empty($propertie['surface_terrain']) ? $propertie['surface_terrain'].' m²' : ''; ?></td>
                        <td data-title="Plans">
                            <a href="<?= SITE . $propertie['plans_paths'][0]['xlarge'] ?>" class="swipebox btn btn-info">
                                Voir Plan
                            </a>
                        </td>
                        <?php if ($propertie['statut']): ?>
                            <td data-title="Demande de devis">
                                <?php if ($propertie['statut']['name'] == 'Disponible'): ?>
                                    <button data-toggle="modal" data-target="#modal-info-<?= $propertie['id']; ?>" class="btn btn-success small">
                                        Disponible
                                    </button>
                                    <div id="modal-info-<?= $propertie['id']; ?>" class="modal modal-message modal-info fade" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3><?= $propertie['projet']['titre'] ?>: <?= $propertie['name'] ?></h3>
                                                </div>
                                                <div class="modal-body">
                                                    <form id="devisForm" name="devisForm-<?= $propertie['id']; ?>" novalidate class="contactForm" method="POST">
                                                        <div class="form-group">
                                                            <label for="name">Nom et prénom:</label>
                                                            <input id="name" type="text" class="form-control" name="name" required data-validation-required-message="Ce champ est obligatoire" />
                                                            <p class="help-block"></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email">Email:</label>
                                                            <input id="email" type="email" class="form-control" name="email" required data-validation-required-message="Ce champ est obligatoire"/>
                                                            <p class="help-block"></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="phone">Téléphone:</label>
                                                            <input id="phone" type="text" class="form-control" name="phone" required data-validation-required-message="Ce champ est obligatoire"/>
                                                            <p class="help-block"></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="bien_name">Bien:</label>
                                                            <input id="bien_name" type="text" value="<?= $propertie['name'] ?>" disabled readonly class="form-control" name="bien_name" required/>
                                                            <p class="help-block"></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="message">Message:</label>
                                                            <textarea id="message" name="message" class="form-control" required data-validation-required-message="Ce champ est obligatoire"></textarea>
                                                            <p class="help-block"></p>
                                                            <input type="hidden" value="<?= $propertie['projet']['titre']; ?>" name="project_name"/>
                                                            <input type="hidden" value="<?= $propertie['projet']['id']; ?>" name="project_id"/>
                                                            <input type="hidden" value="<?= $propertie['name'] ?>" name="name_bien"/>
                                                            <input type="hidden" value="<?= $propertie['id'] ?>" name="id_bien"/>
                                                            <input type="hidden" name="modal_id" value="modal-info-<?= $propertie['id']; ?>">
                                                            <input type="hidden" value="devis-form" name="type-form"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <?= $widget;?>
                                                            <?= $script;?>
                                                            <div id="captcha_error-<?= $propertie['id']; ?>" class="captcha_error"></div>
                                                            <input type="submit" style="display: none" class="myButton1"/>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer text-right">
                                                    <button data-modal-id="modal-info-<?= $propertie['id']; ?>" class="btn btn-info myButton2"  onclick="javascript:submitModal($(this).attr('data-modal-id'))">Envoyer</button>
                                                </div>
                                            </div> <!-- / .modal-content -->
                                        </div> <!-- / .modal-dialog -->
                                    </div>
                                <?php elseif($propertie['statut']['name'] == 'Réservé'): ?>
                                    <button class="btn btn-warning small">
                                        Réservé
                                    </button>
                                <?php else: ?>
                                    <button class="btn btn-danger small">
                                        Vendu
                                    </button>
                                <?php endif; ?>
                            </td>
                        <?php endif; ?>
                        <?php
                            if ($this->uri->segment(3) == '36'||$this->uri->segment(3) == '37'):
                            $url_visite = '';
                            if ($propertie['total_pieces'] == 1):
                                $url_visite = base_url().'visite/mourouj/s+1/';
                            elseif ($propertie['total_pieces'] == 2):
                                $url_visite = base_url().'visite/mourouj/s+2/';
                            elseif ($propertie['total_pieces'] == 3):
                                $url_visite = base_url().'visite/mourouj/s+3/';
                            elseif ($propertie['total_pieces'] == 4):
                                $url_visite = base_url().'visite/mourouj/s+4/';
                            endif;
                            if ($this->uri->segment(3) == '37'):
                                $url_visite = base_url().'visite/centre-urbain-nord/';
                            endif;
                        ?>
                            <td data-title="Viste virtuelle">
                                <?php if($url_visite!=''): ?>
                                <a href="<?= $url_visite; ?>" target="_blank">
                                <button class="btn btn-default small">
                                    Viste virtuelle
                                </button>
                                </a>
                                <?php endif; ?>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <?php if (!empty($links)): ?>
            <?= $links; ?>
        <?php endif; ?>
    </div>
<?php endif; ?>
