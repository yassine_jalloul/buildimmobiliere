<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="short-image no-padding blog-short-title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-12 short-image-title">
                <h5 class="subtitle-margin second-color">ERREUR 404</h5>
                <h1 class="second-color">Page non trouvée</h1>
                <div class="short-title-separator"></div>
            </div>
        </div>
    </div>

</section>

<section class="section-light section-top-shadow">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="huge-header">404<span class="special-color">.</span></h1>
                <h1 class="error-subtitle text-color4">Page non trouvée</h1>
                <p class="margin-top-105 centered-text">La page que vous recherchez a peut-être été supprimée, son adresse a été modifiée ou est devenue temporairement indisponible.</p>
                <p class="centered-text">Aller à la <strong><a href="<?php echo base_url(); ?>">Page d'accueil</a></strong> ou revenir à la <strong><a href="javascript:history.back()">Page précédente</a></strong></p>
            </div>
        </div>
    </div>
</section>