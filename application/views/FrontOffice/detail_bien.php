<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="subheader projet">
</section>
<section class="section-light no-bottom-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <div class="row">
                    <div id="success1"></div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <div class="details-image pull-left hidden-xs">
                            <i class="fa fa-home"></i>
                        </div>
                        <div class="details-title">
                            <h5 class="subtitle-margin"><?php echo $properties['property_type']['name']; ?> pour <?php echo $properties['vocations'][0]['name']; ?></h5>
                            <h3>
                                <?php if (!empty($properties['property_number'])): ?>
                                    N° <?= $properties['property_number']; ?>
                                <?php endif; ?>
                                <?php if (!empty($properties['street'])): ?>
                                    Rue <?= ' ' . $properties['street']; ?>
                                <?php endif; ?>
                                <?php if (!empty($properties['delegation'])): ?>
                                    <?= ' ' . $properties['delegation']['name']; ?>
                                <?php endif; ?>
                                <?php if (!empty($properties['gouvernorat'])): ?>
                                    <?= ' ' . $properties['gouvernorat']['name']; ?>
                                <?php endif; ?>
                                <?php if (!empty($properties['zip_code'])): ?>
                                    <?= ' ' . $properties['zip_code']; ?>
                                <?php endif; ?>
                                <span class="special-color">.</span>
                            </h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 text-right">
                        <div class="button">	
                            <div  data-toggle="modal" data-target="#modal-info" class="button-primary">
                                <span>Contactez nous</span>
                                <div class="button-triangle"></div>
                                <div class="button-triangle2"></div>
                                <div class="button-icon"><i class="fa fa-envelope-o"></i></div>
                            </div>
                        </div>
                        <div id="modal-info" class="modal modal-message modal-info fade" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3><?php echo $properties['property_type']['name'] . ' ' . $properties['name']; ?></h3>
                                    </div>
                                    <div class="modal-body">
                                        <form name="contact-from" id="devisForm" action="<?php echo base_url() ?>contact/send_mail" method="post" novalidate>
                                            <div class="form-group">
                                                <label for="name">Nom et prénom:</label>
                                                <input id="name" type="text" class="form-control" name="name" required data-validation-required-message="Ce champ est obligatoire"/>
                                                <p class="help-block"></p>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email:</label>
                                                <input id="email" type="email" class="form-control" name="mail" required data-validator-validemail-message="Email invalide" data-validation-required-message="Ce champ est obligatoire"/>
                                                <p class="help-block"></p>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Téléphone:</label>
                                                <input id="phone" type="text" class="form-control" name="phone" required data-validation-required-message="Ce champ est obligatoire"/>
                                                <p class="help-block"></p>
                                            </div>
                                            <div class="form-group">
                                                <label for="message">Message:</label>
                                                <textarea id="message" name="message" class="form-control" required data-validation-required-message="Ce champ est obligatoire"></textarea>
                                                <p class="help-block"></p>
                                                <input type="hidden" value="<?php echo current_url(); ?>" name="bien"/>
                                                <input type="hidden" value="devis-form" name="type-form"/>
                                                <input type="submit" style="display:none" id="myButton1" />
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer text-right">
                                        <button type="button" id="myButton2" class="btn btn-info">Envoyer</button>
                                    </div>
                                </div> <!-- / .modal-content -->
                            </div> <!-- / .modal-dialog -->
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 m-b-30">
                        <div class="title-separator-primary"></div>
                    </div>
                    <?php $images = isset($properties['images']) ? $properties['images'] : null; ?>
                    <?php if ($images && count($images)): ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 m-b-20">
                            <!-- Slider main container -->
                            <div id="swiper-gallery" class="swiper-container">
                                <!-- Additional required wrapper -->
                                <div class="swiper-wrapper">
                                    <?php foreach ($images as $image): ?>
                                        <!-- Slides -->
                                        <div class="swiper-slide">
                                            <div class="slide-bg swiper-lazy" data-background="<?php echo SITE . $image['web_path']['xlarge']; ?>"></div>
                                            <!-- Preloader image -->
                                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="slide-buttons slide-buttons-center">
                                    <a href="#" class="navigation-box navigation-box-next slide-next"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe802;</i></div></a>
                                    <div id="slide-more-cont"></div>
                                    <a href="#" class="navigation-box navigation-box-prev slide-prev"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe800;</i></div></a>
                                </div>
                            </div>
                        </div>
                        <div class="thumbs-slider section-both-shadow">
                            <div class="col-xs-1">
                                <a href="#" class="thumb-box thumb-prev pull-left"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe800;</i></div></a>
                            </div>
                            <div class="col-xs-10">
                                <!-- Slider main container -->
                                <div id="swiper-thumbs" class="swiper-container">
                                    <!-- Additional required wrapper -->
                                    <div class="swiper-wrapper">
                                        <?php foreach ($images as $image): ?>
                                            <!-- Slides -->
                                            <div class="swiper-slide">
                                                <img class="slide-thumb" src="<?php echo SITE . $image['web_path']['small']; ?>" alt="">
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1">
                                <a href="#" class="thumb-box thumb-next pull-right"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe802;</i></div></a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="col-xs-12 col-sm-7 col-md-8">
                        <?php if (!empty($properties['description'])): ?>
                            <p class="details-desc"><?= $properties['description']; ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-4 details-desc">
                        <?php if ($properties['prix_visible'] && $properties['prix'] != ''): ?>
                            <div class="details-parameters-price"><?php echo $properties['prix']; ?> DT</div>
                        <?php endif; ?>
                        <div class="details-parameters">
                            <?php if (!empty($properties['reference_immo'])): ?>
                                <div class="details-parameters-cont">
                                    <div class="details-parameters-name">Référence</div>
                                    <div class="details-parameters-val"><?= $properties['reference_immo']; ?></div>
                                    <div class="clearfix"></div>	
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($properties['surface'])): ?>
                                <div class="details-parameters-cont">
                                    <div class="details-parameters-name">Surface.tot</div>
                                    <div class="details-parameters-val"><?php echo $properties['surface']; ?>m<sup>2</sup></div>
                                    <div class="clearfix"></div>	
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($properties['surface_habitable'])): ?>
                                <div class="details-parameters-cont">
                                    <div class="details-parameters-name">Surface.hab</div>
                                    <div class="details-parameters-val"><?php echo $properties['surface_habitable']; ?>m<sup>2</sup></div>
                                    <div class="clearfix"></div>	
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($properties['chambres'])): ?>
                                <div class="details-parameters-cont">
                                    <div class="details-parameters-name">Chambres</div>
                                    <div class="details-parameters-val"><?php echo $properties['chambres']; ?></div>
                                    <div class="clearfix"></div>	
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($properties['sdb_number'])): ?>
                                <div class="details-parameters-cont">
                                    <div class="details-parameters-name">SDB</div>
                                    <div class="details-parameters-val"><?php echo $properties['sdb_number']; ?></div>
                                    <div class="clearfix"></div>	
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($properties['places_parking_number'])): ?>
                                <div class="details-parameters-cont">
                                    <div class="details-parameters-name">Parking</div>
                                    <div class="details-parameters-val"><?php echo $properties['places_parking_number']; ?></div>
                                    <div class="clearfix"></div>	
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-45">
                    <div class="col-xs-12 col-sm-12">
                        <ul class="details-ticks list-inline">
                            <?php
                            foreach ($properties['explained_criteres'] as $key => $critere):
                                if (!empty($critere)):
                                    ?>
                                    <li class="col-md-3"><i class="jfont">&#xe815;</i><?php echo $key; ?></li>
                                    <?php
                                endif;
                            endforeach;
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="row margin-top-45">
                    <div class="col-xs-12 appartement-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tab-map" aria-controls="tab-map" role="tab" data-toggle="tab">
                                    <span>Map</span>
                                    <div class="button-triangle2"></div>
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab-map">
                                <div id="estate-map" class="details-map"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-60">
                    <div class="col-xs-12">
                        <h3 class="title-negative-margin">contactez nos agents<span class="special-color">.</span></h3>
                        <div class="title-separator-primary"></div>
                    </div>
                </div>
                <div class="row margin-top-60">
                    <div class="col-xs-12 col-sm-12">
                        <div class="agent-social-bar">
                            <div class="pull-left">
                                <span class="agent-icon-circle">
                                    <i class="fa fa-phone"></i>
                                </span>
                                <span class="agent-bar-text">(+216) 73 361 278</span>
                            </div>
                            <div class="pull-left">
                                <span class="agent-icon-circle">
                                    <i class="fa fa-envelope fa-sm"></i>
                                </span>
                                <span class="agent-bar-text">direction@dahmenimmobilier.com</span>
                            </div>
                            <div class="pull-right">
                                <div class="pull-right">
                                    <a class="agent-icon-circle" target="_blank" href="https://fr-fr.facebook.com/Dahmaneimmobilier/">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div id="success" class="col-md-12"></div>
                        <form name="contact-from" id="contactForm" action="<?php echo base_url() ?>contact/send_mail" method="post" novalidate>
                            <div class="form-group col-md-6">
                                <input name="name" id="name" type="text" class="form-control main-input input-full" required placeholder="Nom et prénom" />
                                <div class="help-block"></div>
                            </div>
                            <div class="form-group col-md-6">
                                <input name="phone" id="phone" type="text" class="form-control main-input pull-right input-full" placeholder="Téléphone" />
                                <div class="help-block"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <input name="mail" id="mail" type="email" class="form-control main-input input-full" placeholder="Email" />
                                <div class="help-block"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <textarea name="message" id="message" class="form-control main-input input-full" placeholder="Message"></textarea>
                                <input type="hidden" value="contact-form" name="type-form"/>
                                <div class="help-block"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <button type="submit" href="#" class="button-primary pull-right no-border" id="form-submit">
                                    <span>Envoyer</span>
                                    <div class="button-triangle"></div>
                                    <div class="button-triangle2"></div>
                                    <div class="button-icon"><i class="fa fa-paper-plane"></i></div>
                                </button>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php if (!empty($recentProperties)): ?>
                    <div class="row margin-top-90">
                        <div class="col-xs-12 col-sm-9">
                            <h5 class="subtitle-margin">Annonces</h5>
                            <h1>les plus récentes<span class="special-color">.</span></h1>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <a href="#" class="navigation-box navigation-box-next" id="short-offers-owl-next"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe802;</i></div></a>
                            <a href="#" class="navigation-box navigation-box-prev" id="short-offers-owl-prev"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe800;</i></div></a>
                        </div>
                        <div class="col-xs-12">
                            <div class="title-separator-primary"></div>
                        </div>
                    </div>

                    <div class="short-offers-container">
                        <div class="owl-carousel" id="short-offers-owl">
                            <?php foreach ($recentProperties as $recent): ?>
                                <a href="<?php echo base_url(); ?>biens/detail/<?php echo $recent['id']; ?>">
                                    <div class="grid-offer-col">
                                        <div class="grid-offer">
                                            <div class="grid-offer-front">
                                                <?php if (!empty($recent['web_path'])): ?>
                                                    <div class="grid-offer-photo" style="background: url('<?php echo SITE . $recent['web_path']; ?>')">
                                                    <?php else: ?>
                                                        <div class="grid-offer-photo" style="background: url('<?php echo base_url(); ?>/images/default-home.jpg')">
                                                        <?php endif; ?>

                                                        <div class="type-container">
                                                            <div class="transaction-type"><?php echo $recent['vocations'][0]['name']; ?></div>
                                                            <div class="estate-type"><?php echo $recent['property_type']['name']; ?></div>
                                                        </div>
                                                        <?php if (!empty($recent['reference_immo'])): ?>
                                                            <div class="reference"><?= $recent['reference_immo']; ?></div>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="grid-offer-text">
                                                        <?php if (!empty($recent['name'])): ?>
                                                            <h4 class="grid-offer-title couper-mot">
                                                                <?= $recent['name']; ?>
                                                            </h4>
                                                        <?php endif; ?>
                                                        <i class="fa fa-map-marker grid-offer-localization"></i>
                                                        <div class="grid-offer-h4">
                                                            <h4 class="grid-offer-title couper-mot">
                                                                <?php if (!empty($recent['property_number'])): ?>
                                                                    N° <?= $recent['property_number']; ?>
                                                                <?php endif; ?>
                                                                <?php if (!empty($recent['street'])): ?>
                                                                    Rue <?= ' ' . $recent['street']; ?>
                                                                <?php endif; ?>
                                                                <?php if (!empty($recent['delegation'])): ?>
                                                                    <?= ' ' . $recent['delegation']['name']; ?>
                                                                <?php endif; ?>
                                                                <?php if (!empty($recent['gouvernorat'])): ?>
                                                                    <?= ' ' . $recent['gouvernorat']['name']; ?>
                                                                <?php endif; ?>
                                                                <?php if (!empty($recent['zip_code'])): ?>
                                                                    <?= ' ' . $recent['zip_code']; ?>
                                                                <?php endif; ?>
                                                            </h4>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <?php if (!empty($recent['description'])): ?>
                                                            <p><?= truncate_text($recent['description'], 240) . ' [...]' ?></p>
                                                        <?php endif; ?>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="price-grid-cont">
                                                        <div class="grid-price-label pull-left">Prix:</div>
                                                        <?php if ($recent['prix_visible'] && $recent['prix'] != ''): ?>
                                                            <div class="grid-price pull-right">
                                                                <?php echo $recent['prix']; ?> DT
                                                            </div>
                                                        <?php endif; ?>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="grid-offer-params">
                                                        <?php if (!empty($recent['surface'])): ?>
                                                            <div class="grid-area">
                                                                <img src="<?php echo base_url(); ?>images/area-icon.png" alt="" /><?php echo $recent['surface']; ?>m<sup>2</sup>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if (!empty($recent['chambres'])): ?>
                                                            <div class="grid-rooms">
                                                                <img src="<?php echo base_url(); ?>images/rooms-icon.png" alt="" /><?php echo $recent['chambres']; ?>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if (!empty($recent['sdb_number'])): ?>
                                                            <div class="grid-baths">
                                                                <img src="<?php echo base_url(); ?>images/bathrooms-icon.png" alt="" /><?php echo $recent['sdb_number']; ?>
                                                            </div>
                                                        <?php endif; ?>							
                                                    </div>		
                                                </div>
                                                <div class="grid-offer-back">
                                                    <div id="grid-map<?php echo $recent['id']; ?>" class="grid-offer-map"></div>
                                                    <div class="button">	
                                                        <button class="button-primary">
                                                            <span>Lire plus</span>
                                                            <div class="button-triangle"></div>
                                                            <div class="button-triangle2"></div>
                                                            <div class="button-icon"><i class="fa fa-search"></i></div>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="margin-top-45"></div>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="sidebar">
                    <h3 class="sidebar-title">Recherche<span class="special-color">.</span></h3>
                    <div class="title-separator-primary"></div>
                    <div class="sidebar-select-cont">
                        <?php if (!empty($vocations)): ?>
                            <select name="vocation[]" class="bootstrap-select" title="Vocation">
                                <?php foreach ($vocations as $vocation): ?>
                                    <option <?php
                                    if (isset($_GET['vocation']) && $vocation['id'] == $this->input->get('vocation')): echo 'selected';
                                    endif;
                                    ?> value="<?= $vocation['id']; ?>"><?= $vocation['name']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        <?php endif; ?>
                        <?php if (!empty($type_biens)): ?>
                            <select name="property_types[]" class="bootstrap-select" title="Type du bien" multiple data-actions-box="true">
                                <?php foreach ($type_biens as $type_bien): ?>
                                    <option <?php
                                    if (isset($_GET['property_types']) && in_array($type_bien['id'], $_GET['property_types'])): echo 'selected';
                                    endif;
                                    ?> value="<?= $type_bien['id']; ?>"><?= $type_bien['name']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        <?php endif; ?>
                        <?php if (!empty($gouvernorats)): ?>
                            <select name="gouvernorats[]" class="bootstrap-select" title="Gouvernorat" multiple data-actions-box="true">
                                <?php foreach ($gouvernorats as $gouvernorat): ?>
                                    <option <?php
                                    if (isset($_GET['gouvernorats']) && in_array($gouvernorat['id'], $_GET['gouvernorats'])): echo 'selected';
                                    endif;
                                    ?> value="<?php echo $gouvernorat['id']; ?>"><?php echo $gouvernorat['name']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        <?php endif; ?>	
                        <?php if (!empty($delegations)): ?>				
                            <select name="delegations[]" class="bootstrap-select" title="Délégations" multiple data-actions-box="true">
                                <?php foreach ($delegations as $delegation): ?>
                                    <option <?php
                                    if (isset($_GET['delegations']) && in_array($delegation['id'], $_GET['delegations'])): echo 'selected';
                                    endif;
                                    ?> value="<?php echo $delegation['id']; ?>"><?php echo $delegation['name']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        <?php endif; ?>
                    </div>
                    <div class="adv-search-range-cont">	
                        <label for="slider-range-price-sidebar-value" class="adv-search-label">Prix:</label>
                        <span>DT</span>
                        <input type="text" id="slider-range-price-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div
                            data-name="prix"
                            id="slider-range-price-sidebar"
                            data-min="<?= $search['prix_min_property']; ?>"
                            data-max="<?= $search['prix_max_property']; ?>"
                            <?php if (isset($_GET['prix_min']) && is_numeric($this->input->get('prix_min')) && is_numeric($this->input->get('prix_max'))): ?>
                                data-values-min="<?= $this->input->get('prix_min'); ?>"
                                data-values-max="<?= $this->input->get('prix_max'); ?>"
                            <?php else: ?>
                                data-values-min="<?= $search['prix_min_property']; ?>"
                                data-values-max="<?= $search['prix_max_property']; ?>"
                            <?php endif; ?>
                            class="slider-range">
                        </div>
                        <div id="div-data-slider-range-price-sidebar">
                            <?php if (isset($_GET['prix_min']) && is_numeric($this->input->get('prix_min')) && is_numeric($this->input->get('prix_max'))): ?>
                                <input type="hidden" id="input-data-min-slider-range-price-sidebar" name="prix_min" value="<?= $this->input->get('prix_min'); ?>"/>
                                <input type="hidden" id="input-data-max-slider-range-price-sidebar" name="prix_max" value="<?= $this->input->get('prix_max'); ?>"/>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="adv-search-range-cont">	
                        <label for="slider-range-area-sidebar-value" class="adv-search-label">Surface:</label>
                        <span>m<sup>2</sup></span>
                        <input type="text" id="slider-range-area-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div
                            data-name="surface"
                            id="slider-range-area-sidebar"
                            data-min="<?= $search['surface_min_property']; ?>"
                            data-max="<?= $search['surface_max_property']; ?>"
                            <?php if (isset($_GET['surface_min']) && is_numeric($this->input->get('surface_min')) && is_numeric($this->input->get('surface_max'))): ?>
                                data-values-min="<?= $this->input->get('surface_min'); ?>"
                                data-values-max="<?= $this->input->get('surface_max'); ?>"
                            <?php else: ?>
                                data-values-min="<?= $search['surface_min_property']; ?>"
                                data-values-max="<?= $search['surface_max_property']; ?>"
                            <?php endif; ?>
                            class="slider-range">
                        </div>
                        <div id="div-data-slider-range-area-sidebar">
                            <?php if (isset($_GET['surface_min']) && is_numeric($this->input->get('surface_min')) && is_numeric($this->input->get('surface_max'))): ?>
                                <input type="hidden" id="input-data-min-slider-range-area-sidebar" name="surface_min" value="<?= $this->input->get('surface_min'); ?>"/>
                                <input type="hidden" id="input-data-max-slider-range-area-sidebar" name="surface_max" value="<?= $this->input->get('surface_max'); ?>"/>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="adv-search-range-cont">	
                        <label for="slider-range-bedrooms-sidebar-value" class="adv-search-label">Pièces:</label>
                        <input type="text" id="slider-range-bedrooms-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div
                            data-name="pieces"
                            id="slider-range-bedrooms-sidebar"
                            data-min="<?= $search['pieces_min_property']; ?>"
                            data-max="<?= $search['pieces_max_property']; ?>"
                            <?php if (isset($_GET['pieces_min']) && is_numeric($this->input->get('pieces_min')) && is_numeric($this->input->get('pieces_max'))): ?>
                                data-values-min="<?= $this->input->get('pieces_min'); ?>"
                                data-values-max="<?= $this->input->get('pieces_max'); ?>"
                            <?php else: ?>
                                data-values-min="<?= $search['pieces_min_property']; ?>"
                                data-values-max="<?= $search['pieces_max_property']; ?>"
                            <?php endif; ?>
                            class="slider-range">
                        </div>
                        <div id="div-data-slider-range-bedrooms-sidebar">
                            <?php if (isset($_GET['pieces_min']) && is_numeric($this->input->get('pieces_min')) && is_numeric($this->input->get('pieces_max'))): ?>
                                <input type="hidden" id="input-data-min-slider-range-bedrooms-sidebar" name="pieces_min" value="<?= $this->input->get('pieces_min'); ?>"/>
                                <input type="hidden" id="input-data-max-slider-range-bedrooms-sidebar" name="pieces_max" value="<?= $this->input->get('pieces_max'); ?>"/>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!--<div class="adv-search-range-cont">	
                        <label for="slider-range-bathrooms-sidebar-value" class="adv-search-label">Bathrooms:</label>
                        <input type="text" id="slider-range-bathrooms-sidebar-value" readonly class="adv-search-amount">
                        <div class="clearfix"></div>
                        <div id="slider-range-bathrooms-sidebar" data-min="1" data-max="4" class="slider-range"></div>
                    </div>-->
                    <div class="sidebar-search-button-cont">
                        <a href="#" class="button-primary">
                            <span>Trouver</span>
                            <div class="button-triangle"></div>
                            <div class="button-triangle2"></div>
                            <div class="button-icon"><i class="fa fa-search"></i></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>