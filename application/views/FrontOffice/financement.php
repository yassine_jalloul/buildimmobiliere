<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container content-page">
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-title">Financement</h1>
            <p>Ce calcul est sous réserves de vérification auprès de votre banque.</p>
            <ul class="nav nav-tabs financement-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab1">Montant des mensualités</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab2">Capacité d'emprunt</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div id="tab1" class="container tab-pane active"><br>
                    <form name="form1">
                        <div class="form-group">
                            <label>MONTANT DE L'EMPRUNT</label>
                            <input name="montant" type="text" class="form-control col-md-9" placeholder="MONTANT EN DINARS" data-change='true'/>
                        </div>
                        <div class="form-group">
                            <label>AUTO FINANCEMENT</label>
                            <input name="auto_financement" type="text" class="form-control col-md-9" placeholder="AUTO FINANCEMENT EN DINARS" data-change='true'/>
                        </div>
                        <div class="form-group">
                            <label>PÉRIODE</label>
                            <div class="col-md-9 no-padding">
                            <select name="duration" data-change='true' class="form-control">
                                <option value="5">5 ans</option>
                                <option value="10">10 ans</option>
                                <option value="15">15 ans</option>
                                <option value="20">20 ans</option>
                                <option value="25">25 ans</option>
                                <option value="30">30 ans</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>TAUX D'INTERÊT</label>
                            <input name="taux" type="text" class="form-control col-md-9" placeholder="% ( TMM + TAUX DE LA BANQUE )" data-change='true'/>
                        </div>
                        <div class="form-group result" style="display: none">
                            <!--<label>REMBOUSEMENT MENSUEL</label>
                            <input name="result" type="text" class="form-control col-md-9 disabled" readonly placeholder="MONTANT EN DINARS" />-->
                            <ul>
                                <li class="month">Estimation de vos mensualités : <span>0</span></li>
                                <li class="total">Montant total emprunté : <span>0</span></li>
                                <li class="cost">Coût du crédit : <span>0</span></li>
                                <li class="mensuel-revenu">Revenue mensuel minimum: <span>0</span></li>

                            </ul>
                        </div>
                    </form>
                </div>
                <div id="tab2" class="container tab-pane fade"><br>
                    <form name="form2">
                        <div class="form-group">
                            <label>REVENUE MENSUEL</label>
                            <input name="revenue" type="text" class="form-control col-md-9" placeholder="MONTANT EN DINARS" data-change='true'/>
                        </div>
                        <div class="form-group">
                            <label>DURÉE DE L‘EMPRUNT</label>
                            <div class="col-md-9 no-padding">
                            <select name="duration" data-change='true' class="form-control">
                                <option value="5">5 ans</option>
                                <option value="10">10 ans</option>
                                <option value="15">15 ans</option>
                                <option value="20">20 ans</option>
                                <option value="25">25 ans</option>
                                <option value="30">30 ans</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>TAUX D'INTERÊT</label>
                            <input name="taux" type="text" class="form-control col-md-9" placeholder="% ( TMM + TAUX DE LA BANQUE )" data-change='true'/>
                        </div>
                        <!--<div class="form-group">
                            <label>MONTANT DE L‘EMPRUNT</label>
                            <input name="montant" type="text" class="form-control col-md-9 disabled" readonly placeholder="MONTANT EN DINARS" />
                        </div>
                        <div class="form-group">
                            <label>MENSUALITÉS</label>
                            <input name="mensualites" type="text" class="form-control col-md-9 disabled" readonly placeholder="MONTANT EN DINARS (40% DE VOTRE SALAIRE BRUT)" />
                        </div>-->
                        <div class="form-group result" style="display: none">
                            <ul>
                                <li class="amount">Montant de l'emprunt : <span>0</span></li>
                                <li class="mensualites">Estimation de vos mensualités : <span>0</span></li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <a href="<?= base_url('sites/default/files/brochure.pdf'); ?>">
                <img class="img-fluid width-100" src="<?= base_url('images/brochure.jpg'); ?>">
            </a>
            <form name="searchForm" method="GET" action="<?= base_url('search') ?>">
                <h2 class="red-title mt-3">découvrez nos projets</h2>
                <div class="row pt-3 text-center">
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 mb-2">
                        <select name="property_types[]" class="selectpicker  show-tick" title="Type de bien" data-actions-box="true">
                            <?php foreach ($type_biens as $type_bien): ?>
                                <option value="<?= $type_bien['id']; ?>"><?= $type_bien['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <input type="hidden" name="vocations[]" id="vocation" value="1"/>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 mb-2 div-btn-red">
                        <input type="submit" class="btn-red text-uppercase" name="submit" value="Lancer la recherche">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>