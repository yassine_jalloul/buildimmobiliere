<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!empty($properties)):
    foreach ($properties as $propertie):
        ?>
        <div class="col-md-4 property mt-2">
            <a href="<?= SITE . $propertie['plans_paths'][0]['xlarge'] ?>" class="property-img swipebox" style="background: url('<?= SITE . $propertie['plans_paths'][0]['xlarge'] ?>')">
                <div class="img-fade"></div>
                <div class="property-tag button featured"><?= $propertie['property_type']['name'] ?></div>
                <div class="property-tag button status <?= $propertie['statut']['name'] == 'Disponible' ? 'disponible' : ($propertie['statut']['name'] == 'Réservé' ? 'btn-default' : 'vendu') ?>">
                    <?= $propertie['statut']['name'] == 'Disponible' ? Disponible : ($propertie['statut']['name'] == 'Réservé' ? Réservé : Vendu) ?>
                </div>
                <div class="property-color-bar"></div>
            </a>
            <div class="property-content">
                <div class="property-title">
                    <h4>
                        <a href="#">
                            <?php
                            if (!empty($propertie['name'])):
                                echo $propertie['name'];
                            endif;
                            if (!empty($propertie['projet'])):
                                echo ' | ' . $propertie['projet']['titre'];
                            endif;
                            ?>
                        </a>
                    </h4>
                    <p class="property-address"><i class="fa fa-map-marker icon"></i> <?= $propertie['projet']['gouvernorat']['name']; ?></p>
                </div>
                <table class="property-details">
                    <tbody>
                        <tr>

                            <?php
                            $url_visite = '';
                            if (!empty($propertie['total_pieces'])):
                                if ($this->uri->segment(3) == '36'):
                                    if ($propertie['total_pieces'] == 1):
                                        $url_visite = base_url().'visite/mourouj/s+1/';
                                    elseif ($propertie['total_pieces'] == 2):
                                        $url_visite = base_url().'visite/mourouj/s+2/';
                                    elseif ($propertie['total_pieces'] == 3):
                                        $url_visite = base_url().'visite/mourouj/s+3/';
                                    elseif ($propertie['total_pieces'] == 4):
                                        $url_visite = base_url().'visite/mourouj/s+4/';
                                    endif;
                                endif;
                                if ($this->uri->segment(3) == '37'):
                                    $url_visite = base_url().'visite/centre-urbain-nord/';
                                endif;
                                ?>
                                <td><i class="fa fa-bed"></i> S+<?= $propertie['total_pieces']; ?></td>
                            <?php endif; ?>
                            <td>
                                <i class="fa fa-bars"></i> 
                                <?php if ($propertie['etage'] >= 1): ?>
                                    Étage <?= $propertie['etage']; ?>
                                <?php elseif ($propertie['etage'] == 0): ?>
                                    RDC
                                <?php elseif ($propertie['etage'] == -1): ?>
                                    Sous sol
                                <?php endif; ?>
                            </td>
                            <?php if (!empty($propertie['surface_terrain'])): ?>
                                <td><i class="fa fa-expand"></i> <?= $propertie['surface_terrain']; ?> m²</td>
                            <?php endif; ?>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="property-footer">
                <span class="left">
                    <?php if ($propertie['statut']): ?>
                        <?php if ($propertie['statut']['name'] == 'Disponible'): ?>
                        <a href="#" data-toggle="modal" data-target="#modal-info-<?= $propertie['id']; ?>">
                            <i class="fa fa-plus icon"></i> Demande de devis
                        </a>
                        <div id="modal-info-<?= $propertie['id']; ?>" class="modal modal-message modal-info fade" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3><?= $propertie['projet']['titre'] ?>: <?= $propertie['name'] ?></h3>
                                </div>
                                <div class="modal-body">
                                    <form id="devisForm" name="devisForm-<?= $propertie['id']; ?>" novalidate class="contactForm" method="POST">
                                        <div class="form-group">
                                            <label for="name">Nom et prénom:</label>
                                            <input id="name" type="text" class="form-control" name="name" required data-validation-required-message="Ce champ est obligatoire" />
                                            <p class="help-block"></p>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email:</label>
                                            <input id="email" type="email" class="form-control" name="email" required data-validation-required-message="Ce champ est obligatoire"/>
                                            <p class="help-block"></p>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">Téléphone:</label>
                                            <input id="phone" type="text" class="form-control" name="phone" required data-validation-required-message="Ce champ est obligatoire"/>
                                            <p class="help-block"></p>
                                        </div>
                                        <div class="form-group">
                                            <label for="bien_name">Bien:</label>
                                            <input id="bien_name" type="text" value="<?= $propertie['name'] ?>" disabled readonly class="form-control" name="bien_name" required/>
                                            <p class="help-block"></p>
                                        </div>
                                        <div class="form-group">
                                            <label for="message">Message:</label>
                                            <textarea id="message" name="message" class="form-control" required data-validation-required-message="Ce champ est obligatoire"></textarea>
                                            <p class="help-block"></p>
                                            <input type="hidden" value="<?= $propertie['projet']['titre']; ?>" name="project_name"/>
                                            <input type="hidden" value="<?= $propertie['projet']['id']; ?>" name="project_id"/>
                                            <input type="hidden" value="<?= $propertie['name'] ?>" name="name_bien"/>
                                            <input type="hidden" value="<?= $propertie['id'] ?>" name="id_bien"/>
                                            <input type="hidden" name="modal_id" value="modal-info-<?= $propertie['id']; ?>">
                                            <input type="hidden" value="devis-form" name="type-form"/>
                                        </div>
                                        <div class="form-group">
                                            <?= $widget;?>
                                            <?= $script;?>
                                            <div id="captcha_error-<?= $propertie['id']; ?>" class="captcha_error"></div>
                                            <input type="submit" style="display: none" class="myButton1"/>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer text-right">
                                    <button data-modal-id="modal-info-<?= $propertie['id']; ?>" class="btn btn-info myButton2"  onclick="javascript:submitModal($(this).attr('data-modal-id'))">Envoyer</button>
                                </div>
                            </div> <!-- / .modal-content -->
                        </div> <!-- / .modal-dialog -->
                    </div>
                        <?php elseif($propertie['statut']['name'] == 'Réservé'): ?>
                            <a href="javascript:void(0)">
                            Réservé
                        <?php else: ?>
                            <a href="javascript:void(0)">
                            Vendu
                        </a>
                        <?php endif; ?>
                    <?php endif; ?>
                </span>
                <?php if ($url_visite != ''): ?>
                    <span class="right">
                        <a href="<?= $url_visite; ?>" target="_blank">Visite virtuelle</a>
                    </span>
                <?php endif; ?>
                <div class="clearfix"></div>
            </div>
        </div>
        <?php
    endforeach;
    if (!empty($links)):
        echo $links;
    endif;
endif;
?>