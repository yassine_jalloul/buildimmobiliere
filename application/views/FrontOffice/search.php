<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container content-page">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Search</h1>
        </div>
    </div>
    <?php if (!empty($properties)): ?>
        <div class="row">
            <div class="col-md-6 mt-5 mb-3">
                <h2 class="red-title pb-3 mt-3">Liste des plans</h2>
            </div>
            <div class="col-md-6 mt-5 mb-3">											
                <div class="view-icons-container">
                    <?php
                    if ((!empty($_GET)) && ($this->input->get('show') == 'grid')):
                        ?>
                        <a class="view-box" href="<?= $url_search; ?>">
                            <img src="<?= base_url('images/list-icon.png'); ?>" alt=""/>
                        </a>
                        <a href="" class="view-box view-box-active">
                            <img src="<?= base_url('images/grid-icon.png'); ?>" alt=""/>
                        </a>
                    <?php else: ?>
                        <a class="view-box view-box-active" href="">
                            <img src="<?= base_url('images/list-icon.png'); ?>" alt=""/>
                        </a>
                        <a href="" class="view-box">
                            <img src="<?= base_url('images/grid-icon.png'); ?>" alt=""/>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <?php
            $this->load->view($properties_items);
            ?>
        </div>
    <?php endif; ?>
</div>