<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="swipe-div">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <img class="img-fluid" src="<?= base_url('images/home.jpg'); ?>">
                <div class="container slider-content">
                    <div class="caption-slider">
                        <h1 class="name-project-slider">tej el mourouj</h1>
                    </div>
                    <a href="<?= base_url('projets_en_cours/detail/36')?>">
                    <button class="btn btn-slider">Voir le projet</button>
                    </a>
                </div>
            </div>      
            <div class="swiper-slide">
                <img class="img-fluid" src="<?= base_url('images/home2.jpg'); ?>">
                <div class="container slider-content">
                    <div class="caption-slider">
                        <h1 class="name-project-slider">centre urbain nord</h1>
                    </div>
                    <a href="<?= base_url('projets_en_cours/detail/37')?>">
                        <button class="btn btn-slider">Voir le projet</button>
                    </a>
                </div>
            </div>
            <div class="swiper-slide">
                <img class="img-fluid" src="<?= base_url('images/home3.jpg'); ?>">
                <div class="container slider-content">
                    <div class="caption-slider">
                        <h1 class="name-project-slider">ennacer</h1>
                    </div>
                    <a href="<?= base_url('projets_realises/detail/38')?>">
                        <button class="btn btn-slider">Voir le projet</button>
                    </a>
                </div>
            </div>
            <div class="swiper-slide">
                <img class="img-fluid" src="<?= base_url('images/home4.jpg'); ?>">
                <div class="container slider-content">
                    <div class="caption-slider">
                        <h1 class="name-project-slider">sfax</h1>
                    </div>
                    <a href="<?= base_url('projets_avenir/detail/35')?>">
                        <button class="btn btn-slider">Voir le projet</button>
                    </a>
                </div>
            </div>
            <div class="swiper-slide">
                <img class="img-fluid" src="<?= base_url('images/home5.jpg'); ?>">
                <div class="container slider-content">
                    <div class="caption-slider">
                        <h1 class="name-project-slider">dorrat nabeul</h1>
                    </div>
                    <a href="<?= base_url('projets_en_cours/detail/34')?>">
                        <button class="btn btn-slider">Voir le projet</button>
                    </a>
                </div>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>    
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 mt-3">
                    <h2 class="red-title">Découvrez nos projets</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 back-grey  mt-3">
                    <form name="searchForm" method="GET" action="<?= base_url('search') ?>">
                        <div class="row pt-3 text-center">
                            <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12 mb-2">
                                <select name="gouvernorats[]" class="selectpicker  show-tick" title="Gouvernorats" data-actions-box="true">
                                    <option selected value="">Gouvernorats</option>
                                    <?php foreach ($gouvernorats as $gouvernorat): ?>
                                        <option value="<?= $gouvernorat['id']; ?>"><?= $gouvernorat['name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12 mb-2">
                                <select name="property_types[]" class="selectpicker  show-tick" title="Type de bien" data-actions-box="true">
                                    <option selected value="">Type de bien</option>
                                    <?php foreach ($type_biens as $type_bien): ?>
                                        <option value="<?= $type_bien['id']; ?>"><?= $type_bien['name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12 mb-2">
                                <button class="btn-red text-uppercase" type="submit">
                                    Lancer la recherche
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12  mt-3">
                    <h2 class="red-title">Qui sommes nous</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-3">
                    <p><strong>BUILD IMMOBILIERE</strong> est une marque commerciale active dans le secteur immobilier filiale du groupe <strong>MAGASIN GENERAL</strong>, elle est active sur le marché immobilier tunisien à travers ces différentes sociétés :<br />-    Société Hafedh de promotion immobilière,<br />-    Société Zeineb de promotion immobilière,<br />-    Société Immobilière Magasin Général.<br />« Build Immobilière » a pour vocation la réalisation de toute opération de promotion immobilière :<br />-    L’acquisition de terrains sur le territoire tunisien,<br />-    L’édification de différents types de constructions immobilières à vocation commerciale, bureautique, domestique ainsi que de parking à étages.<br />-    La commercialisation.<br />« Build Immobilière », pionnière sur le marché immobilier, offre un service d’excellence grâce à la qualité de ses réalisations, au sérieux de ses performances et au respect de ses engagements.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-3">
                    <h2 class="red-title">Nos Projets</h2>
                </div>
            </div>
            <div class="row projets">
                <div class="col-md-12 mt-3">
                    <div class="owl-carousel">
                        <?php
                        if (!empty($projets)):
                            foreach ($projets as $propertie):
                                ?>
                                <div class="col-md-4 property mt-2 item">
                                    <?php
                                    $url = '#';
                                    if ($propertie['statut']['id'] == 1):
                                        $url = base_url('projets_en_cours/detail/' . $propertie['id']);
                                    elseif ($propertie['statut']['id'] == 2):
                                        $url = base_url('projets_realises/detail/' . $propertie['id']);
                                    else:
                                        $url = base_url('projets_avenir/detail/' . $propertie['id']);
                                    endif;
                                    ?>
                                    <a href="<?= $url; ?>" class="property-img" style="background: url('<?= SITE . $propertie['web_path'] ?>')">
                                        <div class="img-fade"></div>
                                        <?php if (!empty($propertie['count_properties'])): ?>
                                            <div class="property-tag button featured"><?= $propertie['count_properties']; ?> Propriétés</div>
                                        <?php endif; ?>
                                        <div class="property-color-bar"></div>
                                    </a>
                                    <div class="property-content">
                                        <div class="property-title">
                                            <h4>
                                                <a href="<?= $url; ?>">
                                                    <?php
                                                    if (!empty($propertie['titre'])):
                                                        echo $propertie['titre'];
                                                    endif;
                                                    ?>
                                                </a>
                                            </h4>
                                            <p class="property-address"><i class="fa fa-map-marker icon"></i> <?= $propertie['gouvernorat']['name']; ?></p>
                                            <?php if (!empty($propertie['description'])): ?>
                                                <div class="mt-2 property-description">
                                                    <?= truncate_text($propertie['description'], 350) . ' ...' ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>