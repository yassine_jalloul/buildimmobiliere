<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6Lc3RIoUAAAAAMQ5s-ZQXBit2uDnwVwISxAmfhRB';
$config['recaptcha_secret_key'] = '6Lc3RIoUAAAAAJIXVLsge2XNP1xGr6fXL9r6SL6m';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'fr';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
