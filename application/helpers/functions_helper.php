<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function truncate_text($string, $limit, $break = ' ', $pad = '') {
    mb_internal_encoding('UTF-8');
    if (mb_strlen($string) <= $limit + mb_strlen($pad))
        return $string;

    $string = mb_substr($string, 0, $limit);
    if (false !== ($breakpoint = mb_strrpos($string, $break)))
        $string = mb_substr($string, 0, $breakpoint);

    return trim($string) . $pad;
}

function Get_all_projects() {
    $gets['_token'] = IMMO_TOKEN;
    $gets['num_items_per_page'] = 9;
    $gets['order'] = 'ordre';
    $gets['srt'] = 'ASC';
    $url = SITE . 'api/projets';
    $query = http_build_query($gets);
    $url .= "?$query";
    //echo $url;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    $result = utf8_decode($result);
    return json_decode($result, true);
}

function Project_by_id($id) {
    $gets['_token'] = IMMO_TOKEN;
    $url = SITE . 'api/' . $id . '/projet-details';
    $query = http_build_query($gets);
    $url .= "?$query";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    $result = utf8_decode($result);
    return json_decode($result, true);
}
function curl_properties_detail($id){
    $gets['id'] = $id;
    $gets['_token'] = IMMO_TOKEN;
    $url = SITE . 'api/property-details';
    $query = http_build_query($gets);
    $url .= "?$query";
    $ret = curl_custom_init($url);
    return $ret;
}
function Properties_by_project($id, $page) {
    $gets['_token'] = IMMO_TOKEN;
    $gets['num_items_per_page'] = 9;
    if ($page != 1):
        $gets['page'] = $page;
    endif;
    $url = SITE . 'api/' . $id . '/properties-by-project';
    $query = http_build_query($gets);
    $url .= "?$query";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    $result = utf8_decode($result);
    return json_decode($result, true);
}

function CURL_PARAM_SEARCH() {
    $gets['_token'] = IMMO_TOKEN;
    $url = SITE . 'api/params-search';
    $query = http_build_query($gets);
    $url .= "?$query";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    $result = utf8_decode($result);
    return json_decode($result, true);
}

function curl_search($params, $page, $id) {
    $gets['_token'] = IMMO_TOKEN;
    $url = SITE . 'api/search';
    $gets['num_items_per_page'] = 9;
    $gets['projet'] = $id;
    if ($page != 1):
        $gets['page'] = $page;
    endif;
    $gets = array_merge($gets, $params);
    $query = http_build_query($gets);
    $url .= "?$query";
    //echo $url;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    $result = utf8_decode($result);
    return json_decode($result, true);
}

function curl_search_by_id_project($id) {
    $url = SITE . 'api/params-search-by-project';
    $gets['_token'] = IMMO_TOKEN;
    $gets['projetId'] = $id;
    $query = http_build_query($gets);
    $url .= "?$query";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    $result = utf8_decode($result);
    return json_decode($result, true);
}

function curl_properties_draw_api($project = null)
{
    $gets['_token'] = IMMO_TOKEN;
    $url = SITE . 'api/draw-property-list';
    $query = http_build_query($gets);
    $url .= "?$query";
    $result = curl_custom_init($url);
    $properties = array();
    foreach ($result as $r){
        if($project && $project == $r['info']['project']){
            $properties[] = $r;
        }
    }
    return $properties;
}
function curl_custom_init($url){

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36',
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "Content-Type: application/x-www-form-urlencoded; charset=utf-8",
            "x-auth-token: ".IMMO_TOKEN
        )
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    $result = utf8_decode($response);
    return json_decode($result, true);

}
function pagination() {
    $config['per_page'] = 9;
    $config['num_links'] = 2;
    $config["uri_segment"] = 4;
    $config['num_links'] = 2;
    $config['use_page_numbers'] = TRUE;
    $config['reuse_query_string'] = TRUE;
    $config['full_tag_open'] = '<div class="item-list col-md-12 text-center"><ul class="pager">';
    $config['full_tag_close'] = '</ul></div>';

    $config['first_link'] = '« First';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_link'] = 'Last »';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_link'] = 'Next ›';
    $config['next_tag_open'] = '<li class="pager-next">';
    $config['next_tag_close'] = '</li>';


    $config['cur_tag_open'] = '<li class = "pager-current">';
    $config['cur_tag_close'] = '</li>';

    $config['prev_link'] = '‹ Previous';
    $config['prev_tag_open'] = '<li class="pager-previous">';
    $config['prev_tag_close'] = '</li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    return $config;
}

function CURL_send_mail($url, $data) {
    $gets['_token'] = IMMO_TOKEN;
    $url = SITE . $url;
    $query = http_build_query($gets);
    $url .= "?$query";
    //echo $url;
    $curl = curl_init();
    //
    curl_setopt($curl, CURLOPT_POST, 1);
    if ($data['type-form'] == 'devis'):
        $POSTFIELDS = array(
            'name' => $data['name'],
            'email' => $data['email'],
            'tel' => $data['tel'],
            //'message' => $data['message'],
            'propertyId' => $data['propertyId'],
            'projetId' => $data['projetId']
        );
    else:
        $POSTFIELDS = array(
            'name' => $data['name'],
            'email' => $data['email'],
            'subject' => $data['subject'],
            //'message' => $data['message'],
            'content' => $data['content'],
                //'projetId' => $data['projetId']
        );
    endif;
    curl_setopt($curl, CURLOPT_POSTFIELDS, $POSTFIELDS);
    //
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    $result = utf8_decode($result);
    return json_decode($result, true);
}
