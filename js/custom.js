/**
 * Created by ANDOLSI on 18/12/2018.
 */
function load_map(container_id, title, lat, lng, zoom) {
    zoom = zoom || 13;
    var map = L.map('map-container-projet').setView([lat, lng], zoom);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    if(title){
        L.marker([lat, lng]).addTo(map)
            .bindPopup(title)
            .openPopup();
    }
}
