function senddevisForm(t, e) {
    var s = t.serialize();
    var s1 = t.serializeArray();
    var data1 = [];
    $(s1).each(function(index, obj){
        data1[obj.name] = obj.value;
    });
    //console.log(data['modal_id']);
    var modal_id = data1['modal_id'];
    var id_bien = data1['id_bien'];
    $.ajax({
        url: baseweb + "contact/send_mail",
        type: "POST",
        data: s,
        cache: !1,
        success: function (response) {
            var obj = JSON.parse(response);
            //console.log(obj);
            if(obj.hasOwnProperty('status')) {
                if (obj.status == 1) {
                    console.log(obj.status);
                    $('#' + modal_id).modal('toggle');
                    $.notify({
                        // options
                        message: '<strong>Votre message a bien été envoyé, nous reviendrons vers vous dans les plus prompts délais.</strong>'
                    }, {
                        // settings
                        type: 'info'
                    });
                    $("#"+modal_id+" .contactForm").trigger("reset");
                    grecaptcha.reset();
                } else if(obj.status == 0) {
                    console.log(obj.status);
                    $.notify({
                        // options
                        message: '<strong>Désolé, il semble que notre serveur de messagerie ne répond pas. Veuillez réessayer plus tard!</strong>'
                    }, {
                        // settings
                        type: 'danger'
                    });
                    $("#"+modal_id+" .contactForm").trigger("reset");
                    grecaptcha.reset();
                } else{
                    console.log(obj.status);
                    $("#"+modal_id+' .captcha_error').html("<div class='alert alert-danger'>");
                    $("#"+modal_id+' .captcha_error > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $("#"+modal_id+' .captcha_error > .alert-danger').append("<strong>Captcha error");
                    $("#"+modal_id+' .captcha_error > .alert-danger').append('</div>');
                    $("#"+modal_id+' .captcha_input').val('');
                    grecaptcha.reset();

                }
            }
        },
        error: function () {
            $.notify({
                // options
                message: '<strong>Désolé, il semble que notre serveur de messagerie ne répond pas. Veuillez réessayer plus tard!</strong>'
            }, {
                // settings
                type: 'danger'
            });
        }
    });
}
function sendcontactForm(t, e) {
    var s = t.serialize();
    $.ajax({
        url: baseweb + "contact/send_mail",
        type: "POST",
        data: s,
        cache: !1,
        success: function (response) {

            var obj = JSON.parse(response);
            //console.log(obj);
            if(obj.hasOwnProperty('status')) {
                if (obj.status == 1) {
                    $.notify({
                        // options
                        message: '<strong>Votre message a bien été envoyé, nous reviendrons vers vous dans les plus prompts délais.</strong>'
                    }, {
                        // settings
                        type: 'info'
                    });
                    $("#contactForm").trigger("reset");
                    grecaptcha.reset();
                } else if(obj.status == 0) {
                    $.notify({
                        // options
                        message: '<strong>Désolé, il semble que notre serveur de messagerie ne répond pas. Veuillez réessayer plus tard!</strong>'
                    }, {
                        // settings
                        type: 'danger'
                    });
                    $("#contactForm").trigger("reset");
                    grecaptcha.reset();
                } else{
                    $('#captcha_error').html("<div class='alert alert-danger'>");
                    $('#captcha_error > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#captcha_error > .alert-danger').append("<strong>Captcha error");
                    $('#captcha_error > .alert-danger').append('</div>');
                    $('#captcha_input').val('');
                    grecaptcha.reset();

                }
            }

        },
        error: function () {
            $.notify({
                // options
                message: '<strong>Désolé, il semble que notre serveur de messagerie ne répond pas. Veuillez réessayer plus tard!</strong>'
            }, {
                // settings
                type: 'danger'
            });
        }
    });
}
$(function () {
    $(".contactForm input,.contactForm textarea,.contactForm select").jqBootstrapValidation({
        preventSubmit: !0,
        submitError: function (t, e, s) {},
        submitSuccess: function (t, e) {
            //console.log(t.attr('action'));
            e.preventDefault();
            var s = $(e.target).attr("id");
            "devisForm" === s && senddevisForm(t, e);
            "contactForm" === s && sendcontactForm(t, e);
        },
        filter: function (t) {
            //console.log(t.attr('action'));
            return $(this).is(":visible");
        }
    });
    $('a[data-toggle="tab"]').click(function (t) {

        t.preventDefault(), $(this).tab("show");
    });
});