var swiper = new Swiper('.swiper-container', {
    centeredSlides: true,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});

// Regular map
// function regular_map() {
//     var var_location = new google.maps.LatLng(36.805420, 10.182983);
//
//     var var_mapoptions = {
//         center: var_location,
//         zoom: 14
//     };
//
//     var var_map = new google.maps.Map(document.getElementById("map-container"),
//             var_mapoptions);
//
//     var var_marker = new google.maps.Marker({
//         position: var_location,
//         map: var_map,
//         title: "Siège Groupe MG"
//     });
// }
function mapInitAgency() {
    var zoom = zoom || 14;
    var title = title || "";
    var ui = false;
    var map = L.map('map-container', {
        zoomControl:ui,
        scrollWheelZoom: ui,
        doubleClickZoom: ui,
        dragging: ui,
    }).setView([36.805420, 10.182983], zoom);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    var piniconCustom = L.icon({
        iconUrl: 'images/pin-empty.png',
        iconSize: [41, 51], // size of the icon
        iconAnchor: [20, 51], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -51] // point from which the popup should open relative to the iconAnchor
    });

    if(title){
        L.marker([36.805420, 10.182983], {icon: piniconCustom}).addTo(map)
            .bindPopup(title)
            .openPopup();
    } else {
        L.marker([36.805420, 10.182983], {icon: piniconCustom}).addTo(map)
    }
}
// Initialize maps
google.maps.event.addDomListener(window, 'load', mapInitAgency);


$(document).ready(function () {
    $('.selectpicker').selectpicker();
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 54)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });
});