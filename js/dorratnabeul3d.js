var available_properties = [];
var threed = null;
var xhr = null;
var fullscreen = false;
var arr_duplex = ["penthouse", "duplex"];
var hovered_property = null;
var selected_property = null;
properties.forEach(function (G, k) {
	if ("info" in G) {
		if ("etage" in G.info) {
			if (G.info.etage >= 0) {
				available_properties.push(G.info.reference);
			}
		}
	}
});
function load_3d() {
    $('#root').toggle();
    threed = new App({
        containerId: 'root',
        object3D: obj3D,
        objectMtl3D: objMtl3D,
        sounds: {
            background: backgroundSound,
            click: clickSound
        },
        objectDir3D: objDir3D,
        textures: textures,
        properties: properties,
		localisation: [36.811865, 10.141920],
        propertyMouseOut: function(data, event){
            hideTooltip();
        },
        propertyMouseIn: function(data, event){
            //showTooltip(data, event);
        },
        propertyOnClick: function (data, event) {
            selected_property = data;
            showTooltip(data, event, true);
            if(is_mobile){
                hideSearch();
            }
        },
        propertyClick: function (data, event, open) {
            if (open) {
                openDetailProperty(data);
            }
        },
		loaded: function(){
			$('.button-toolbar').show();
		}
    });
    $('.before-load').remove();
}
$(document).ready(function () {

	properties.forEach(function (G, k) {
		if ("coords" in G) {
			if (G["coords"].length) {
				if ("info" in G) {
					if ("etage" in G.info) {
						if (G.info.etage >= 0) {
							available_properties.push(G.info.title);
						}
					}
				}
			}
		}
	});
	initSearchFrom();
});

function submitSearchForm() {
	$('form.SearchFormDetail:eq(0)').submit();
}

function initSearchFrom() {
	$("form.SearchFormDetail:eq(0) input[name='reference']").autocomplete({
		source: available_properties,
		select: function (event, ui) {
			var terms = ui.item.value;
			this.value = terms;
			submitSearchForm();
		},
		appendTo: "#full-container-3d"
	}).keyup(function () {
		if (!this.value) {
			submitSearchForm();
		}
	});
	$('form.SearchFormDetail:eq(0) select').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
		submitSearchForm();
	});
	$('form.SearchFormDetail:eq(0)').submit(function () {
		var data = getFormData($(this));
		config3D.search(data);
	})
}

function getFormData($form) {
	var unindexed_array = $form.serializeArray();
	var indexed_array = {};
	
	$.map(unindexed_array, function (n, i) {
		indexed_array[n['name']] = n['value'];
	});
	
	return indexed_array;
}

function openDetailProperty(item) {
	if ("info" in item) {
		if ("id" in item.info) {
			$('#showModalDetail3 .modal-content').append("<div class=\"loader-container\"><div class=\"loader\"></div></div>");
			$('#showModalDetail3').modal("show");
			if (xhr) {
				xhr.abort()
			}
			xhr = $.ajax({
				url: baseweb + "ajax/detail/" + item.info.id,
				data: {},
				type: "GET",
				success: function (data) {
					$('#showModalDetail3 .modal-content').html(data);
					$("#showModalDetail3 form input[name=property]").val(item.info.title);
					$("#showModalDetail3 form input[name=id_property]").val(item.info.id);
					$('.swipebox').swipebox();
                    $('.selectpicker').selectpicker();
				},
				error: function () {
					$('#showModalDetail3').modal("hide");
				}
			}).fail(function () {
				$('#showModalDetail3').modal("hide");
			})
		}
	}
}

function resetFromSearch() {
	$('form.SearchFormDetail:eq(0) select').each(function (e) {
		if ($(this).attr("name") == "etage") {
			$(this).val(-2);
		} else {
			$(this).val("");
		}
	});
	$('form.SearchFormDetail:eq(0) input').val("");
	$('form.SearchFormDetail:eq(0) select').selectpicker('refresh');
	hideTooltip();
	submitSearchForm();
}

function goFullScreen() {
	if (fullscreen) {
		closeFullscreen();
		fullscreen = false;
	} else {
		openFullscreen();
		fullscreen = true;
	}
}

/* View in fullscreen */
function openFullscreen() {
	var elem = document.getElementById('full-container-3d');
	if (elem.requestFullscreen) {
		elem.requestFullscreen();
	} else if (elem.mozRequestFullScreen) { /* Firefox */
		elem.mozRequestFullScreen();
	} else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
		elem.webkitRequestFullscreen();
	} else if (elem.msRequestFullscreen) { /* IE/Edge */
		elem.msRequestFullscreen();
	}
}

/* Close fullscreen */
function closeFullscreen() {
	var elem = document.getElementById('full-container-3d');
	if (document.exitFullscreen) {
		document.exitFullscreen();
	} else if (document.mozCancelFullScreen) { /* Firefox */
		document.mozCancelFullScreen();
	} else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
		document.webkitExitFullscreen();
	} else if (document.msExitFullscreen) { /* IE/Edge */
		document.msExitFullscreen();
	}
}

function openSearchModal(){
	$("#searchModal3D").modal("show");
}
function openSettingModal(){
	$("#settingModal3D").modal("show");
}
function showTooltip(data, event, pos=false){
	if(data){
		if ("info" in data) {
			if(data.info){
				if("id" in data.info){
					var txt = getTooltipContent(data);
					if(txt){
						$("#full-container-3d .ui-tooltip-custom").html(txt);
						var evt = event.data.originalEvent;
						var x = evt.pageX - $('#wrapper').offset().left;
						var y = evt.pageY - $('#wrapper').offset().top;
						y += 20;
						$("#full-container-3d .ui-tooltip-custom").css("position", "absolute");
						if(pos){
							if(is_mobile){
								$("#full-container-3d .ui-tooltip-custom").css("top", "0%");
								$("#full-container-3d .ui-tooltip-custom").css("right", "0%");
							} else {
								$("#full-container-3d .ui-tooltip-custom").css("top", 34 + "%");
								$("#full-container-3d .ui-tooltip-custom").css("left", 75 + "%");
							}
							
						} else {
							$("#full-container-3d .ui-tooltip-custom").css("top", y + "px");
							$("#full-container-3d .ui-tooltip-custom").css("left", x + "px");
						}
						$("#full-container-3d .ui-tooltip-custom").css("margin", 'auto');
						$("#full-container-3d .ui-tooltip-custom").css("z-index", 1);
						$("#full-container-3d .ui-tooltip-custom").show();
						return ;
					}
					
				}
			}
			
		}
	}
	hideTooltip();
}

function hideTooltip(){
	$("#full-container-3d .ui-tooltip-custom").hide();
}


function getTooltipContent(item){
	var txt = '';
	if ("info" in item) {
		if(item.info){
			txt = "<div  class='title-tooltip'>" + item.info.title + "<button type=\"button\" onclick='hideTooltip()' class='close-tooltip'><span aria-hidden=\"true\">&times;</span></button></div>";
			if ("image" in item.info) {
				if ("small" in item.info.image) {
					txt += "<img class='hidden-xs' src='" + item.info.image.small + "'><br/>";
				}
			}
			txt += "<div class='clearfix'></div>";
			txt += "<div class='description'>";
			if ("property_type" in item.info) {
				if(item.info.property_type){
					var subType = "";
					if ("property_sub_type" in item.info) {
						if(item.info.property_sub_type.length) {
							item.info.property_sub_type.forEach(function (e) {
								if(arr_duplex.indexOf(e.toLowerCase())>= 0 && !subType){
									subType = e;
								}
							})
						}
					}
					txt += "<p>" + "<strong>Type: </strong>" +item.info.property_type + (subType ? " " + subType : "") + "</p>";
				}
			}
			var clsNonAvailable = "alert-danger"
			if ("statut" in item.info) {
				if ("available" in item.info) {
					if (item.info.available) {
						clsNonAvailable = "alert-success";
					}
				}
				txt += "<p><strong>Statut:</strong> <span class='"+clsNonAvailable+"' role=\"alert\">" + item.info.statut + "</span></p>";
			} else {
				txt += "<p><strong>Statut:</strong> <span class='"+clsNonAvailable+"' role=\"alert\">Non disponible</span></p>";
			}
			if ("etage" in item.info) {
				txt += "<p>" + "<strong>Etage: </strong>" +(item.info.etage ?  + item.info.etage : "RDC") + "</p>";
			}
			if ("pieces" in item.info) {
				if(item.info.pieces) {
					txt += "<p><strong>Pieces:</strong> S+" + item.info.pieces + "</p>";
				}
			}
			if ("surface_habitable" in item.info) {
				txt += "<p><strong>Surface habitable:</strong> " + item.info.surface_habitable + " m²</p>";
			}
			if ("surface_terrain" in item.info) {
				txt += "<p><strong>Surface terrain:</strong> " + item.info.surface_terrain + " m²</p>";
			}
			if ("surface_terrasse" in item.info) {
				if(item.info.surface_terrasse){
					txt += "<p><strong>Surface terrasse:</strong> " + item.info.surface_terrasse + " m²</p>";
				}
			}
			if ("surface_jardin" in item.info) {
				if(item.info.surface_jardin) {
					txt += "<p><strong>Surface jardin:</strong> " + item.info.surface_jardin + " m²</p>";
				}
			}
			if ("surface_piscine" in item.info) {
				if(item.info.surface_piscine) {
					txt += "<p><strong>Surface piscine:</strong> " + item.info.surface_piscine + " m²</p>";
				}
			}
			txt += "<p class='colored'><a href='#' onclick='openDetailProperty(selected_property); return false;'>Cliquer sur le bien pour plus de détails</a></p>";
			txt += "</div>";
		}
	}
	return txt;
}

function playScene(button){
	button = $(button);
	if(button.find('i:eq(0)').hasClass('fa-play')){
		button.find('i:eq(0)').removeClass('fa-play');
		button.find('i:eq(0)').addClass('fa-pause');
	} else {
		button.find('i:eq(0)').removeClass('fa-pause');
		button.find('i:eq(0)').addClass('fa-play');
	}
	config3D.play();
}
function toggleSound(button){
	button = $(button);
	var i = button.find('i:eq(0)');
	if(i.hasClass('fa-volume-off')){
		i.removeClass('fa-volume-off');
		i.addClass('fa-volume-up');
	} else {
		i.removeClass('fa-volume-up');
		i.addClass('fa-volume-off');
	}
	config3D.toggleSound();
}


function toggleCompass(button){
	config3D.toggleCompass();
}
function openTimeModal(button){
	$('#timeModal3D').modal('show')
}
function showFormContact() {
	$('#devisForm1 .text-danger, #devisForm1 #error, #devisForm1 #success').html("");
	if ($("#devisForm1").is(":hidden")) {
		$("#devisForm1").slideDown("slow");
	} else {
		$("#devisForm1").slideUp("slow");
	}
}
function submitContactProperty(form) {
	form = $(form);
	var serializeArr = form.serializeArray();
	var serializeStr = form.serialize();
	var status = true;
	$('#showModalDetail3 form .text-danger, #showModalDetail3 form #error, #showModalDetail3 form #success').html("");
	serializeArr.forEach(function(element){
		if (element.name) {
			if (!element.value) {
				$('#showModalDetail3 form [name="' + element.name + '"]').next(".text-danger").html("Ce champ est obligatoir!")
				status = false;
			}
		}
	});
	if (status) {
		$('#showModalDetail3 .modal-content').append("<div class=\"loader-container\"><div class=\"loader\"></div></div>");
		$.ajax({
			url: baseweb + "ajax/senddevis/",
			data: serializeStr,
			type: "POST",
			success: function (data) {
                var obj = JSON.parse(data);
                if(obj.hasOwnProperty('status')) {
                    if (obj.status) {
                        //$('#showModalDetail3 form #success').html("Email envoyé!");
                        setCookie('senddevis','1');
                        window.location = baseweb + 'projets/success';
                    } else {
                        $('#showModalDetail3 form #error').html("error !");
                    }
                    $("#showModalDetail3 .modal-content .loader-container").remove();
                }
			},
			error: function () {
				$("#showModalDetail3 .modal-content .loader-container").remove();
			}
		}).fail(function () {
			$("#showModalDetail3 .modal-content .loader-container").remove();
		})
	}
	return false;
}

function openVisite360(){
	$('.visite-360').fadeIn(400);
}
function closeVisite360(){
	$('.visite-360').fadeOut(200);
}
function setCookie(cname, cvalue) {
    var d = new Date();
    d.setTime(d.getTime() + 1000*60);
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function ShowHideSearchForm() {
    if($('#icon-search').hasClass('d-none')){
        $('#icon-search').removeClass('d-none');
    }else{
        $('#icon-search').addClass('d-none');
    }
    $('.SearchFormDiv').toggle("slide");
}